#!/usr/bin/python

import os
import subprocess

"""Fetch the .po files from KDE's SVN for KDE.org

Run me from KDE.org top-level directory.
"""

SVN_PRE_PATH = "svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5"
SVN_SUB_PATH = "messages/websites-kde-org"
PO_PATH = "pos"

if not os.path.exists(PO_PATH):
    os.mkdir(PO_PATH)

# all_languages = "af ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia crh cs csb cy da de el en en_GB eo es et eu fa fi fr fy ga gd gl gu ha he hl hne hr hsb hu hy ia id is it ja ka kk km kn ko ku lb lt lv mai mk ml mr ms mt nb nds ne nl nn nso oc or pa pl ps pt pt_BR ro ru rw se si sk sl sq sr sr@ijekavian sr@ijekavianlatin sr@latin sv ta te tg th tn tr tt ug uk uz uz@cyrillic vi wa xh zh_CN zh_HK zh_TW".split(" ")
all_languages = "ast ca ca@valencia cs es et eu fr fi hu it nl gl pt pt_BR ko ru sk sv uk".split(" ")

for lang in all_languages:
    SVN_PATH = "{}/{}/{}".format(SVN_PRE_PATH, lang, SVN_SUB_PATH)
    OUTPUT = "{}/{}".format(PO_PATH, lang)
    try:
        subprocess.check_output(['svn', 'export', SVN_PATH, OUTPUT], stderr=subprocess.PIPE)
        print("{} fetched".format(lang))
    except subprocess.CalledProcessError:
        print("{} non-existent".format(lang))
