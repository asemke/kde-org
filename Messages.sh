#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-LicenseIdentifier: BSD-2-Clause

python3 translations.py extract $podir
