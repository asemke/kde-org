---
title: "Donation received - Thank you!"
---

# Donations

Thank you very much for your donation to the KDE project!

Remember you can become a "KDE Supporting Member" by doing recurring donations. Learn more at <a href="https://relate.kde.org/">relate.kde.org/</a>.

You can see your donation in the [list](/community/donations/previousdonations).
