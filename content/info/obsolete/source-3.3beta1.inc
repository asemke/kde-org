<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/arts-1.2.91.tar.bz2">arts-1.2.91</a></td>
   <td align="right">964kB</td>
   <td><tt>8a094aa369b2218078a42d4896ab165d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdeaccessibility-3.2.91.tar.bz2">kdeaccessibility-3.2.91</a></td>
   <td align="right">1.7MB</td>
   <td><tt>3f2fd72951ccd6b22555d62f0f3367cd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdeaddons-3.2.91.tar.bz2">kdeaddons-3.2.91</a></td>
   <td align="right">1.9MB</td>
   <td><tt>ed4d58da93a964b0e6f047788f03337f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdeadmin-3.2.91.tar.bz2">kdeadmin-3.2.91</a></td>
   <td align="right">2.0MB</td>
   <td><tt>a691c287fe4b790ed4c69b961bf970a2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdeartwork-3.2.91.tar.bz2">kdeartwork-3.2.91</a></td>
   <td align="right">17MB</td>
   <td><tt>b7496260c940b668e920b72b50b715a2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdebase-3.2.91.tar.bz2">kdebase-3.2.91</a></td>
   <td align="right">17MB</td>
   <td><tt>04162d1f0b915e966b719562c9e45e3d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdebindings-3.2.91.tar.bz2">kdebindings-3.2.91</a></td>
   <td align="right">7.0MB</td>
   <td><tt>755c2d04f38ca352b9cfd4d0ba8ad7af</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdeedu-3.2.91.tar.bz2">kdeedu-3.2.91</a></td>
   <td align="right">20MB</td>
   <td><tt>a6e2c5280195feaa53ead56eebb33270</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdegames-3.2.91.tar.bz2">kdegames-3.2.91</a></td>
   <td align="right">9.5MB</td>
   <td><tt>15cb445db633a1aeafd37d201c2bf0c3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdegraphics-3.2.91.tar.bz2">kdegraphics-3.2.91</a></td>
   <td align="right">6.3MB</td>
   <td><tt>c2f5917a0e46fe072d596714a47e250f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kde-i18n-3.2.91.tar.bz2">kde-i18n-3.2.91</a></td>
   <td align="right">179MB</td>
   <td><tt>a5dac68d683c7aca8a4414942885ea3b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdelibs-3.2.91.tar.bz2">kdelibs-3.2.91</a></td>
   <td align="right">15MB</td>
   <td><tt>2d8eadb6ff2e15c57b4462872deb67b3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdemultimedia-3.2.91.tar.bz2">kdemultimedia-3.2.91</a></td>
   <td align="right">5.6MB</td>
   <td><tt>df70dd138b15a24aa185299bd5da7643</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdenetwork-3.2.91.tar.bz2">kdenetwork-3.2.91</a></td>
   <td align="right">6.9MB</td>
   <td><tt>27d3f6de495646fda09736d1816ce492</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdepim-3.2.91.tar.bz2">kdepim-3.2.91</a></td>
   <td align="right">9.0MB</td>
   <td><tt>1e1db631f0238c381b183d206299e0f0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdesdk-3.2.91.tar.bz2">kdesdk-3.2.91</a></td>
   <td align="right">4.4MB</td>
   <td><tt>3cbda8523e98e28bacae7c89de5e96ba</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdetoys-3.2.91.tar.bz2">kdetoys-3.2.91</a></td>
   <td align="right">6.5MB</td>
   <td><tt>f4d15bfebfedc564b8db4245fb92b18b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdeutils-3.2.91.tar.bz2">kdeutils-3.2.91</a></td>
   <td align="right">2.6MB</td>
   <td><tt>d615a848b52053acf5bd94b1c9e1b1c3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdevelop-3.0.91.tar.bz2">kdevelop-3.0.91</a></td>
   <td align="right">7.8MB</td>
   <td><tt>16fdfcd5a32ba4c191e3d369d6f5c0d6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/3.2.91/src/kdewebdev-3.2.91.tar.bz2">kdewebdev-3.2.91</a></td>
   <td align="right">4.9MB</td>
   <td><tt>a282d8cb3ed4f6a9931e95a82b986610</tt></td>
</tr>

</table>
