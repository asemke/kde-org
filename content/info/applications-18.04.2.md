---
version: "18.04.2"
title: "KDE Applications 18.04.2 Info Page"
announcement: /announcements/announce-applications-18.04.2
type: info/application-v1
build_instructions: https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
---

