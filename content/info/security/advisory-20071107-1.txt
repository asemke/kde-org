
KDE Security Advisory: kpdf/kword/xpdf multiple xpdf based vulnerabilities
Original Release Date: 2007-11-07
URL: http://www.kde.org/info/security/advisory-20071107-1.txt

0. References
         CVE-2007-4352
         CVE-2007-5392
         CVE-2007-5493

1. Systems affected:

        KDE 3.2.0 up to including KDE 3.5.8.
        All KOffixe 1.x releases.


2. Overview:

        kpdf, the KDE pdf viewer, shares code with xpdf. xpdf contains
        multiple vulnerabilities that can crash kpdf or possibly
        execute arbitrary code. The issues were reported by Secunia
        Research. Similiar xpdf based code also exists in kword
        pdf import filters of KOffice 1.x.


3. Impact:

        Remotely supplied pdf files can be used to disrupt the kpdf
        viewer on the client machine and possibly execute arbitrary code.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patch for KOffice 1.6.3 and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        1d6b58dbe1cab4b0168a8e4344f8d7af  koffice-1.6.3-xpdf2-CVE-2007-4352-5392-5393.diff


        Patch for KDE 3.5.8 and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        eee08cb565c24ad645aad3136de5162d  post-3.5.8-kdegraphics-kpdf.diff


        Patch for KDE 3.5.5 and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        b2214c8e8f6300e72bd92f9ba12a33c7  post-3.5.5-kdegraphics-CVE-2007-5393.diff

        Patch for KDE 3.4.3 and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        e7ff688250cee0e1d81f4d65fc0f9871  post-3.4.3-kdegraphics-CVE-2007-5393.diff


