-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: Temporary Directory Vulnerability
Original Release Date: 2004-08-11
URL: http://www.kde.org/info/security/advisory-20040811-1.txt

0. References

        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0689


1. Systems affected:

        All versions of KDE up to KDE 3.2.3 inclusive. 


2. Overview:

        The SUSE security team was alerted that in some cases the
        integrity of symlinks used by KDE are not ensured and that
        these symlinks can be pointing to stale locations. This can
        be abused by a local attacker to create or truncate arbitrary
        files or to prevent KDE applications from functioning
        correctly (Denial of Service).

        KDE creates in ~/.kde symlinks to a temporary directory, a socket
        directory and a cache directory. When a user logs into the KDE
        environment the startkde script ensures that these symlinks are
        present and point to directories that are owned by the user.
        However, when a user runs KDE applications outside the KDE
        environment or when a user runs a KDE applications as another user, 
        such as root, the integrity of these symlinks is not checked and it
        is possible that a previously created but now stale symlinks exist.

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2004-0689 to this issue.


3. Impact:

        When a stale symlink is present a local attacker could create the
        directory that the symlink is pointing to with his own credentials
        to prevent access to this directory by KDE applications. This can
        prevent KDE applications from functioning correctly.

        When a stale symlink is present a local attacker could create the
        directory that the symlink is pointing to with his own credentials.
        Since KDE applications will attempt to create files with certain
        known names in this directory, an attacker can abuse this to overwrite
        arbitrary files with the privileges of the user.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patches for KDE 3.0.5b are available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

  da950a651e69cd810019efce284120fc  post-3.0.5b-kdelibs-kstandarddirs.patch

        Patches for KDE 3.1.5 are available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

  c97ab0cf014adb59e315047210316f5d  post-3.1.5-kdelibs-kstandarddirs.patch

        Patches for KDE 3.2.3 are available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

  345ce2e01cfdfa4754c47894c0271dcc  post-3.2.3-kdelibs-kstandarddirs.patch


6. Time line and credits:


        23/06/2004 SUSE Security Team alerted by Andrew Tuitt
	26/06/2004 Patches created
	27/07/2004 Vendors notified
        11/08/2004 Public advisory

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.2 (GNU/Linux)

iD8DBQFBGioUN4pvrENfboIRAnALAJ9ynwVAnzRtkDghmItkkCTe8qu/eACfabZc
X/9KZihVfSQKjOHvmvBOzv0=
=VM4l
-----END PGP SIGNATURE-----
