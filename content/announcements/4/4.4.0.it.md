---
aliases:
- ../4.4
date: '2010-02-09'
title: KDE SC 4.4.0 Caikaku Release Announcement
---

<div style=" text-align: justify;">
<h3 align="enter">
  KDE Software Compilation 4.4.0 Introduce una nuova interfaccia per i Netbook, capacità di organizzazione delle finestre per schede e un nuovo sistema di autenticazione.
</h3>

<p align="justify">
  <strong>
    Rilasciata KDE Software Compilation 4.4.0 (Nome in codice: <i>"Caikaku"</i>)
  </strong>
</p>

<p align="justify">
Oggi  <a href=http://www.kde.org/>KDE</a> annuncia la disponibilità immediata di KDE Software Compilation 4.4, nome in codice <i>"Caikaku"</i>, la quale porta con se un innovativo insieme di applicazioni agli utenti del Free Software. Importanti nuove tecnologie sono state introdotte, tra le quali social networking e strumentazioni per la collaborazione on line, una nuova interfaccia ottimizzata per i Netbook e innovazioni infrastrutturali come il nuovo sotto-sistema di autenticazione. Stando al sistema di mantenimento dei bug di KDE, 7293 bug sono stati risolti e 1433 richieste di nuove funzionalità sono state implementate. La comunità KDE desidera ringraziare tutti coloro i quali hanno dato una mano a far si che questo importante rilascio fosse possibile.
</p>
</div>

<div class="text-center">
	<a href="/announcements/4/4.4.0/general-desktop.png">
	<img src="/announcements/4/4.4.0/thumbs/general-desktop_thumb.png" class="img-fluid" alt="Lo spazio di lavoro KDE Plasma">
	</a> <br/>
	<em>Lo spazio di lavoro KDE Plasma</em>
</div>
<br/>

<p align=justify>
 Per maggiori dettagli sui miglioramenti leggete <a href="/announcements/4/4.4.0/guide">la guida visuale</a> a KDE Software Compilation 4.4 oppure continuate a leggere qui per un riassunto.
</p>

<h3>
  Lo spazio di lavoro Plasma guadagna capacità di interazione sociale e per il web
</h3>
<br />
<p align=justify>
 Lo spazio di lavoro KDE Plasma offre le funzionalità di base di cui l'utente ha bisogno per iniziare a gestire le proprie applicazioni, file e impostazioni. Gli sviluppatori dello spazio di lavoro KDE Plasma hanno aggiunto una nuova interfaccia studiata apposta per i Netbook, migliorato la grafica e semplificato l'interazione con l'utente. L'introduzione di servizi on line e sociali interfacciati con il web, nasce dalla natura collaborativa della comunità KDE quale un team dedito allo sviluppo di software libero.
</p>

<div class="text-center">
	<em>Interazione migliorata in Plasma </em><br/>
<a href="http://blip.tv/file/get/Sebasje-KDEPlasmaIn44540.ogg">scarica il video</a></div>
<br/>

<p align="justify">
<ul>
  <li>
    <strong>Plasma Netbook</strong> è al suo debutto nella versione 4.4.0. Plasma Netbook è una interfaccia alternativa al Desktop Plasma, specificatamente studiata per un utilizzo più ergonomico nei netbook. Il framework Plasma è stato pensato fin dall'inizio per essere flessibile anche al di fuori del concetto classico di Desktop. L'interfaccia Plasma per i netbook condivide molte componenti con il classico Desktop Plasma, ma è specificatamente scritto e per fare un uso ottimale del poco spazio disponibile nei monitor più piccoli, ed è anche più facile da utilizzare nei dispositivi con touchscreen.
    Plasma Netbook avvia le applicazioni direttamente a schermo intero e fornisce una barra per la ricerca e una visualizzazione a due colonne che offre la possibilità di disporre al meglio i Plasmoidi per la visualizzazione di contenuti on line e altri strumenti provenienti dal Desktop classico.
  </li>
  <li>
    L'iniziativa di <strong>Desktop sociale</strong> apporta miglioramenti nel plasmoide di OpenDesktop.org (prima conosciuto come Social Desktop widget), dando la possibilità agli utenti di mandare e ricevere messaggi e trovare persone vicine direttamente dal Desktop. Il nuovo Plasmoide per le notizie dalla comunità fornisce aggiornamenti in tempo reale di quello che succede nei social network e un altro nuovo Plasmoide permette di effettuare ricerche in vari database come la Knowledge Base di openDesktop.org.
  </li>
  <li>
    La nuova funzionalità di raggruppamento delle finestre in schede di KWin dà la possibilità all'utente di <strong>raggruppare le finestre</strong> all'interno di un'interfaccia a schede, rendendo più semplice la gestione di situazioni che richiedono molte finestre aperte. Ulteriori miglioramenti nella gestione delle finestre comprendono la possibilità di disporre o massimizzare le finestre semplicemente trascinandole verso un lato dello schermo. La squadra di sviluppo di KWin ha cooperato con gli sviluppatori di Plasma per poter migliorare l'interazione tra le applicazioni, aggiungendo così animazioni più fluide e prestazioni migliori. Inoltre è ora possibile per gli artisti creare e condividere i temi per le finestre in maniera più semplice grazie all'introduzione di funzionalità come il supporto alla grafica vettoriale e una più completa configurabilità del gestore dei temi.
  </li>
</ul>
</p>

<div class="text-center">
	<em>Più semplicità per la gestione delle finestre  </em><br/>
<a href="http://blip.tv/file/get/Sebasje-WindowManagingFeaturesInKDEPlasma44222.ogg">scarica il video</a></div>
<br/>

<h3>
  Innovazione nelle applicazioni KDE
</h3>
<p align=justify>
La comunità KDE mette a disposizione un elevato numero di facili ma potenti applicazioni. Questo rilascio introduce novità e miglioramenti per alcune di esse.
</p>
<p align=justify>
<ul>
  <li>
    Con questo rilascio della KDE Software Compilation l'interfaccia <strong>GetHotNewStuff</strong> è stata largamente migliorata come risultato di un lungo processo di progettazione e pianificazione. Questo strumento è stato progettato per permettere alla grande comunità di contributori esterni al progetto KDE di <strong>essere facilmente collegati</strong> con i milioni di utenti dei loro contenuti. Gli utenti possono infatti scaricare dati come <a href="http://dimsuz.wordpress.com/2009/11/11/katomic-new-feature-level-sets-support/">nuovi livelli di KAtomic</a>, nuove stelle o nuove funzionalità direttamente dalle stesse applicazioni. Una novità è costituita anche dalle <strong>funzionalità sociali</strong> come la possibilità di commentare, valutare e diventare fan di un prodotto il quale mostrerà i relativi aggiornamenti nel plasmoide Social News. Gli utenti possono ora <strong>caricare il risultato</strong> dei loro lavori creativi direttamente dalle applicazioni, evitando il noioso processo di pacchettizzazione e pubblicazione sul sito.
  </li>
  <li>
    Altri due progetti di lungo termine della comunità KDE fanno capolino in una veste usabile in questo rilascio. Nepomuk, frutto di un progetto di ricerca internazionale finanziato dalla Comunità Europea ha raggiunto un sufficiente livello di stabilità e prestazioni. <strong><a href="http://ppenz.blogspot.com/2009/11/searching.html">Le funzionalità di ricerca di Dolphin</a> </strong> fanno uso di Nepomuk nell'aiutare l'utente ad organizzare e trovare i propri dati. La nuova visualizzazione temporale mostra i file che l'utente ha utilizzato recentemente in una vista cronologica. Nel frattempo la squadra KDE PIM ha ultimato i lavori per l'adattamento della prima applicazione facente uso del nuovo sistema di gestione e archiviazione dei dati personali, <strong>Akonadi</strong>. La rubrica dei contatti di KDE, KAddressBook, è stata infatti riscritta con una interfaccia a tre pannelli e fa ora uso di Akonadi per la gestione dei dati. Una migrazione sostanziale delle altre applicazioni a queste tecnologie seguirà nei prossimi rilasci di KDE Software Compilation.
  </li>
  <li>
    Inoltre, con l'inserimento di queste nuove tecnologie, i vari team di sviluppo hanno migliorato e integrato le loro applicazioni in vari modi. Gli sviluppatori di KGet hanno aggiunto il supporto per il controllo automatico delle firme digitali e la possibilità di scaricare un file da sorgenti diverse, Gwenview ora include un comodo strumento di importazione delle immagini. In aggiunta, applicazioni nuove o completamente riscritte vengono rilasciate in questa occasione come Palapeli, il classico gioco del puzzle, che da la possibilità all'udente di creare e condividere dei puzzle personalizzati, e Cantor, un'interfaccia facile ed intuitiva per software scientifici come <a href="http://r-project.org/">R</a>, <a href="http://sagemath.org/">SAGE</a>, e <a href="http://maxima.sourceforge.net/">Maxima</a>. Nel mondo KDE PIM è invece stato introdotto Blogilo, un nuovo programma per la gestione dei blog.
  </li>
</ul>
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/dolphin-search.png">
	<img src="/announcements/4/4.4.0/thumbs/dolphin-search_thumb.png" class="img-fluid" alt="Ricerca desktop integrata nel file manager Dolphin">
	</a> <br/>
	<em>Ricerca desktop integrata nel file manager Dolphin</em>
</div>
<br/>

<h3>
  La piattaforma che accelera lo sviluppo
</h3>
<p align=justify>
L'importanza che la comunità KDE ha sempre dato alle suddette nuove tecnologie ha fatto si che la piattaforma KDE divenisse una delle più complete, consistenti ed efficienti piattaforme di sviluppo esistenti. Le librerie di sviluppo per la versione 4.4.0 introducono molte novità dal punto di vista di sfruttamento delle tecnologie di collaborazione on line e utilizzo dei social networks. La piattaforma KDE fornisce alternative potenti, aperte e flessibili a tecnologie esistenti. Nessun impegno è stato profuso nel tentativo di limitare gli utenti alle tecnologie KDE. Al contrario, sono stati fatti sforzi nell'innovare l'esperienza on line come quella sul Desktop.
<p align=justify>
<ul>
  <li>
    Anche le fondamenta di KDE hanno visto miglioramenti e innovazioni. Prime su tutte le librerie <strong>Qt</strong>, giunte alla versione <strong>4.6</strong>. La nuova versione delle <strong>Qt</strong> rende questo ambiente di sviluppo disponibile per la piattaforma mobile Symbian, un nuovo framework per le animazioni, supporto al multitouch e prestazioni migliorate. Le nuove tecnologie di <strong>Social Desktop</strong> introdotte nel precedente rilascio, sono state migliorate con una gestione centralizzata delle identità e con l'introduzione delle librerie <strong>libattica</strong> è stato reso più semplice l'adattamento a diversi servizi web. <strong>Nepomuk</strong> fa ora uso di un backend molto più stabile e performante, rendendolo la scelta giusta per ogni necessità di indicizzazione di meta-dati e ricerca.
  </li>
  <li>
    <strong>KAuth fornisce un nuovo, sicuro metodo di autenticazione</strong> con le relative interfacce utente e la strumentazione per gli sviluppatori di applicazioni che richiedono privilegi elevati. Su Linux, KAuth utilizza PolicyKit, fornendo così una <strong>trasparente integrazione fra ambienti desktop differenti</strong>. KAuth è già utilizzato in alcune finestre di dialogo nelle impostazioni di sistema, ma verrà integrato anche nel Desktop Plasma e nelle altre applicazioni nei prossimi mesi. KAuth supporta l'assegnazione e la revoca delle autorizzazioni in maniera granulare, garantendo una facile gestione delle politiche, memorizzazione delle password e un numero di elementi grafici da utilizzare nelle applicazioni per semplificare il suo utilizzo.
  </li>
  <li>
    <strong>Akonadi</strong>, la soluzione libera per la gestione dei dati personali, viene introdotta ufficialmente tra le applicazioni di KDE Software Compilation. L'applicazione per la gestione della rubrica, KAddressBook, è infatti la prima applicazione ad utilizzare questa nuova tecnologia. KAddressBook, utilizzando Akonadi, supporta sia rubriche locali che remote tramite vari server groupware. KDE SC 4.4.0 segna l'inizio dell'era di Akonadi, con molte applicazioni in arrivo a sfruttare queste nuove tecnologie. Akonadi diventerà l'interfaccia principale per la gestione di email, calendario, rubrica sia in locale che attraverso server groupware remoti o servizi on line. Pubblicato sotto la licenza LGPL (che ne permette l'utilizzo nello sviluppo di applicazioni open source e proprietarie) e cross-platform (disponibile quindi su Linux, UNIX, Mac OS e MS Windows).
  </li>
</ul>
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/social-web-widgets.png">
	<img src="/announcements/4/4.4.0/thumbs/social-web-widgets_thumb.png" class="img-fluid" alt="Il Web e i Social Networks sul Desktop Plasma">
	</a> <br/>
	<em>Il Web e i Social Networks sul Desktop Plasma</em>
</div>
<br/>

<h4>
Altri cambiamenti
</h4>
<p align=justify>
Come detto, quanto descritto in precedenza rappresenta solo una selezione delle novità e migliorie al Desktop, alle applicazioni e alla piattaforma di sviluppo di KDE. Un elenco molto più esauriente, ma comunque incompleto, può essere trovato nei <a href="http://techbase.kde.org/Schedules/KDE4/4.4_Feature_Plan">piani di sviluppo per KDE SC 4.4</a> su <a href="http://techbase.kde.org">TechBase</a>. Informazioni sulle applicazioni che non sono sviluppate come parte ufficiale di KDE SC possono essere trovate sulla <a href="https://www.kde.org/family/">KDE family webpage</a> e su <a href="http://kde-apps.org">kde-apps.org</a>.
</p>

<h4>
    Spargi la parola e vedi che succede
</h4>
<p align="justify">
La comunità KDE incoraggia tutti a <strong>spargere la parola</strong> sul web sociale. Inviate i vostri articoli su siti di notizie, usate canali come digg, reddit, twitter, identi.ca. Caricate screenshot su servizi come Facebook, Flickr, ipernity e Picasa e aggiungeteli ai gruppi appropriati. Create screencast e caricateli su siti come YouTube, Blip.tv, Vimeo o altri. Non dimenticate di taggare il materiale caricato con il <em>tag <strong>kde</strong></em> rendendo così più semplice per tutti la ricerca di materiale coerente, e per la squadra di KDE la compilazione del rapporto della copertura mediatica raggiunta dell'annuncio di KDE SC 4.4. <strong>Aiutaci a spargere la voce, sii parte di essa!</strong></p>

<p align="justify">
Puoi seguire quello che succede sul web sociale a proposito del rilascio di KDE SC 4.4 sul nuovo <a href="http://buzz.kde.org"><strong>KDE Community livefeed</strong></a>. Questo sito aggrega tutto quello che succede su identi.ca, twitter, youtube, flickr, picasaweb, blog e molti altri social network in tempo reale. Il livefeed si trova su <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_Software_Compilation_4_4_0_Introduces_new_innovations"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/technology/comments/azx29/kde_software_compilation_440_introduces_netbook/"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde44"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde44"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde44"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde44"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde44"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 5pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>

<h4>
  Installare KDE SC 4.4.0
</h4>
<p align="justify">
KDE, con le sue librerie e applicazioni è disponibile liberamente sotto licenze Open Source. Il software KDE gira su vari tipi di hardware e sistemi operativi e funziona con ogni tipo di gestore delle finestre o ambiente desktop. Come le versioni per Linux e altri UNIX, potete trovare anche le versioni di molte applicazioni KDE anche per Microsoft Windows sulla pagina <a href="http://windows.kde.org">KDE on Windows</a> e anche versioni per Mac OS X sulla pagina <a href="http://mac.kde.org/">KDE on Mac site</a>. Versioni sperimentali di applicazioni KDE per sistemi operativi mobili come MS Windows Mobile o Symbian possono essere trovate in rete ma sono attualmente non supportate.
<br />
KDE può essere ottenuto sotto forma di codice sorgente e veri formati precompilati su <a href="http://download.kde.org/stable/4.4.0/">http://download.kde.org</a> ma può essere anche ottenuto su <a href="http://www.kde.org/download/cdrom">CD-ROM</a> o con ognuna delle distribuzioni <a href="http://www.kde.org/download/distributions">GNU/Linux e UNIX</a> disponibili oggi.
</p>
<p align="justify">
  <em>Pacchettizzazione</em>.
  Alcuni fornitori di distribuzioni Linux/UNIX OS hanno gentilmente messo a disposizione pacchetti precompilati di KDE SC 4.4.0 per alcune versioni delle loro distribuzioni. In altri casi lavoro analogo è stato fatto da volontari. <br />
  Alcuni di questi pacchetti sono disponibili liberamente su <a href="http://download.kde.org/binarydownload.html?url=/stable/4.4.0/">http://download.kde.org</a>.
  Altri pacchetti precompilati, come anche aggiornamenti a quelli esistenti, verranno resi disponibili nelle prossime settimane.
</p>

<p align="justify">
  <a id="package_locations"><em>Disponibilità dei pacchetti</em></a>.
  Per una lista aggiornata dei pacchetti disponibili dei quali la comunità KDE è stata informata visitare la <a href="/info/4.4.0">pagina di informazioni su KDE SC 4.4.0</a>.
</p>

<h4>
  Compilare KDE SC 4.4.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  Il codice sorgente completo di KDE SC 4.4.0 può essere <a href="http://download.kde.org/stable/4.4.0/src/">liberamente scaricato</a>.
Istruzioni su come compilare ed installare KDE SC 4.4.0
sono disponibili nella <a href="/info/4.4.0#binary">pagina di informazioni su KDE SC 4.4.0</a>.
</p>
</div>



