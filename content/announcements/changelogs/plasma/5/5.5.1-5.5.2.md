---
aliases:
- /announcements/plasma-5.5.1-5.5.2-changelog
hidden: true
plasma: true
title: Plasma 5.5.2 complete changelog
type: fulllog
version: 5.5.2
---

### <a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a>

- Set a proper icon for the notifier plasmoid. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=29e2eae4b7b840bf201c5a23febfebf1029a9591'>Commit.</a>
- Fix type conversion issue. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=abc04a52618cd9a4be40a3ea3a2b4e79069930ff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356726'>#356726</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- Remove unused cmake statements. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=5f6ada75b7c83e8aabdb5bcb8df5af2bc891beb5'>Commit.</a>

### <a name='kwayland' href='http://quickgit.kde.org/?p=kwayland.git'>KWayland</a>

- Fix OutputDevice::edid(). <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=8d0debab24e4d875619a7c143de1c415b9f1e0a3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126380'>#126380</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- Fix build with Qt 5.6. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=05c542ad602a114751f34ac9d03597f11e95470f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126234'>#126234</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Behave properly in popups again. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8e4431a2c05ba12ffb8c771c1ea0c842ec18453b'>Commit.</a>
- Fix multi-row margin tweak for vertical orientation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2eb1f696846b27380d76aa105cc2ffaaebb6548e'>Commit.</a>
- Use tight bounding rect so we don't get a jump /up/ in size while scaling down. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8c353f73376c9fa9781206c62073e82b02fcb97a'>Commit.</a>
- Change taskmanager applet TextLabel's font size when switch to oxygen theme. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=830c5802dc63b5ec3da3ea6d6fd88b18583be3ae'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126412'>#126412</a>
- Don't let the panel controller put the panel into a bad-looking state. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8ba36332272cd8adb29da4dd2f435eacda9c4c79'>Commit.</a>
- [Desktop ToolBox] Hide ToolBox popup when ToolBox is disabled. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2efefd360c76e5cb454e54deecb3d45b7407945f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354682'>#354682</a>
- Add spacing between rows. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7e9695855a7d9756ab9d033e6777d32b063fe6d9'>Commit.</a>
- Reduce margin between icon and text. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2007860b6e672a66f8102d569f24024d3512edbd'>Commit.</a>
- Make initial default panel thickness scale with DPI. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=eed2f0206184a5066038a6dea7402f24634fb72e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126363'>#126363</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- [Containment Actions] Send along proper wheel orientation. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c8e71c462c0aa4f449239771b10f56a4894e8f62'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356799'>#356799</a>. Code review <a href='https://git.reviewboard.kde.org/r/126432'>#126432</a>