Dir: kdeartwork
----------------------------
new version numbers



Dir: kdeartwork/debian
----------------------------
Fix source overrides (see #261435).
----------------------------
Split kscreensaver into two separate packages: kscreensaver and kscreensaver-xsavers.  Enough is enough.
----------------------------
More manpage updates for 3.3.
----------------------------
Final changes for kdeartwork.
----------------------------
Oops.
----------------------------
Updates for 3.3.1.



Dir: kdeartwork/kwin-styles/icewm
----------------------------
Backport shade fixes.



Dir: kdeartwork/kwin-styles/kstep
----------------------------
Backport shade fixes.



Dir: kdeartwork/kwin-styles/plastik/Attic
----------------------------
Backport shade fixes.



Dir: kdeartwork/styles/plastik/Attic
----------------------------
backport: do not stop all animations just because one progressbar gets hidden
----------------------------
backport: the ProgressBar animation timer should not run when the widget isn't visible
