------------------------------------------------------------------------
r1235447 | rkcosta | 2011-06-06 06:26:19 +1200 (Mon, 06 Jun 2011) | 10 lines

Make the 0..9 buttons untranslatable.

I don't think any language actually translates them, but it doesn't
hurt, given the issues we faced in the related bug.

Backport of r1235446.

CCBUG: 256591


------------------------------------------------------------------------
r1235740 | lueck | 2011-06-09 09:28:56 +1200 (Thu, 09 Jun 2011) | 3 lines

Fix dbus call to clear Run Command History
CCMAIL:202811
REVIEW:6707
------------------------------------------------------------------------
r1236147 | lueck | 2011-06-12 02:30:46 +1200 (Sun, 12 Jun 2011) | 4 lines

fix to clear 'Closed Items History'
REVIEW:6708
BUG:183197
FIXED-IN:4.6.5
------------------------------------------------------------------------
r1238192 | scripty | 2011-06-24 15:41:28 +1200 (Fri, 24 Jun 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1239250 | scripty | 2011-07-04 16:15:21 +1200 (Mon, 04 Jul 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1240128 | teran | 2011-07-07 06:17:29 +1200 (Thu, 07 Jul 2011) | 4 lines

backport of fix       

BUG: 276568

------------------------------------------------------------------------
