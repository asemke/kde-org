------------------------------------------------------------------------
r1181414 | jsimon | 2010-10-01 12:21:59 +1300 (Fri, 01 Oct 2010) | 4 lines

Backport r1181413 from trunk:

Somehow a QPixmap on Mac OS doesn't support alpha blending

------------------------------------------------------------------------
r1181445 | scripty | 2010-10-01 15:50:38 +1300 (Fri, 01 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1181474 | mueller | 2010-10-01 20:58:13 +1300 (Fri, 01 Oct 2010) | 2 lines

fix FINAL build

------------------------------------------------------------------------
r1181638 | fdekruijf | 2010-10-02 04:34:05 +1300 (Sat, 02 Oct 2010) | 1 line

update
------------------------------------------------------------------------
r1181739 | alexmerry | 2010-10-02 12:01:04 +1300 (Sat, 02 Oct 2010) | 3 lines

Backport r1181738: Avoid an infinite loop in some cases when there is only one item in the block


------------------------------------------------------------------------
r1181741 | alexmerry | 2010-10-02 12:24:07 +1300 (Sat, 02 Oct 2010) | 3 lines

More efficient way to implement the fix in r1181739.


------------------------------------------------------------------------
r1182570 | dfaure | 2010-10-05 12:43:11 +1300 (Tue, 05 Oct 2010) | 3 lines

Backport fix for 170806 crash to 4.5 branch
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1182764 | adawit | 2010-10-06 04:21:48 +1300 (Wed, 06 Oct 2010) | 1 line

Backported commits r1180984 and r1181322 from trunk
------------------------------------------------------------------------
r1182766 | cfeck | 2010-10-06 04:32:52 +1300 (Wed, 06 Oct 2010) | 4 lines

Fix sizeHint of KNumInput (backport r1181110)

CCBUG: 221989

------------------------------------------------------------------------
r1182815 | lueck | 2010-10-06 07:21:29 +1300 (Wed, 06 Oct 2010) | 1 line

documentation backport from trunk for 4.5.3
------------------------------------------------------------------------
r1182902 | dfaure | 2010-10-06 11:51:07 +1300 (Wed, 06 Oct 2010) | 2 lines

backport fix for unittest failure

------------------------------------------------------------------------
r1183232 | jlayt | 2010-10-07 05:14:24 +1300 (Thu, 07 Oct 2010) | 8 lines

Fix init of Calendar Era if using min/max date.

Backport of r1182931

Thanks Michael for spotting that!

CCMAIL: mpyne@kde.org

------------------------------------------------------------------------
r1183317 | dfaure | 2010-10-07 11:33:23 +1300 (Thu, 07 Oct 2010) | 4 lines

Also assume https when the .desktop has %u and no list of supported protocols.
FIXED-IN: 4.5.3
BUG: 253294

------------------------------------------------------------------------
r1183496 | orlovich | 2010-10-08 04:55:10 +1300 (Fri, 08 Oct 2010) | 5 lines

Merged revision:r1183493 | orlovich | 2010-10-07 11:42:15 -0400 (Thu, 07 Oct 2010) | 4 lines

Fix rounding issues with border sizes

BUG: 252280
------------------------------------------------------------------------
r1183527 | dfaure | 2010-10-08 06:32:02 +1300 (Fri, 08 Oct 2010) | 4 lines

Backport: Apply patch from [Bug 253526] [PATCH] konsole closes its input pty at times -> stops input/output and crashes
FIXED-IN: 4.5.3
BUG: 253526

------------------------------------------------------------------------
r1183578 | dfaure | 2010-10-08 12:20:47 +1300 (Fri, 08 Oct 2010) | 3 lines

Backport SVN commit 1180508 by dfaure: Fix helper protocols such as mailto: and telnet:, patch by bcooksley.
Thanks to kdepepo for noticing that it was not a trunk-only bug, since r1172681 has been backported to 4.5 branch.

------------------------------------------------------------------------
r1183776 | aseigo | 2010-10-09 00:35:52 +1300 (Sat, 09 Oct 2010) | 3 lines

hide(QGraphicsWidget*) results in the destruction of d->tipWidget
CCBUG:253387

------------------------------------------------------------------------
r1183779 | rempt | 2010-10-09 00:39:23 +1300 (Sat, 09 Oct 2010) | 4 lines

Fix memory leak.

See http://svn.reviewboard.kde.org/r/5549/#review8035

------------------------------------------------------------------------
r1183796 | rempt | 2010-10-09 01:38:09 +1300 (Sat, 09 Oct 2010) | 4 lines

Don't leak memory when calling open more than once

See http://svn.reviewboard.kde.org/r/5536/

------------------------------------------------------------------------
r1183890 | dfaure | 2010-10-09 04:41:29 +1300 (Sat, 09 Oct 2010) | 2 lines

Backport commits 1183876 and 1183885

------------------------------------------------------------------------
r1183938 | dfaure | 2010-10-09 08:31:13 +1300 (Sat, 09 Oct 2010) | 16 lines

Fix assertion "listers.isEmpty() || killed" (or "HUH? Lister ... is supposed to be listing" in debug mode)
after a dirlister was put into the wrong list, in the following situation (happening at k3b startup)
1) A flat dirlister lists the contents of /a/b/, so this goes into the cache
2) A tree dirmodel for '/' tries to drill down to /a/b: it lists /a, then /a/b, finds items in the cache, creates a job
  to emit them, and then goes back to "Directory listing finished for /a, are we done with that dir?", but the code was not
  comparing urls, so because of the pending cached-item-job, it thought it was still listing that dir, and didn't mark it as done.
  Then the job for /a/b would be processed, but that didn't fix the status of /a.
3) And only when doing something else later on (e.g. deleting /a/foo, which triggers an update of /a), the inconsistency
would show up and assert (lister marked as "still listing /a" but no associated job).

Writing the unittest: 2h. Fixing the bug: 10mn. Writing this description: 20mn :-)
With thanks to Jaime Torres for finding a way to reproduce the crash very reliably!

FIXED-IN: 4.5.3
BUG: 193364

------------------------------------------------------------------------
r1184870 | orlovich | 2010-10-12 05:48:31 +1300 (Tue, 12 Oct 2010) | 8 lines

Merged revision:r1184868 | orlovich | 2010-10-11 12:47:15 -0400 (Mon, 11 Oct 2010) | 7 lines

Add HTMLDocument::activeElement. Got requested in a bug report, and it's in HTML5, too.
Also cleanup completely redundant HTMLDocumentImpl::body() (which was identical(!)) to 
DocumentImpl::body())

BUG: 253847
FIXED-IN: 4.5.3
------------------------------------------------------------------------
r1184881 | dfaure | 2010-10-12 06:28:26 +1300 (Tue, 12 Oct 2010) | 3 lines

Dump more information on stderr when dir=0, which seems to be the case in the valgrind of bug 197851
CCBUG: 197851

------------------------------------------------------------------------
r1184883 | dfaure | 2010-10-12 06:34:18 +1300 (Tue, 12 Oct 2010) | 4 lines

Also dump more info when the crash from 213895 is about to happen - and make it more robust in release mode, only assert in debug mode.
This will be in 4.5.3, please paste the stderr from the app if this happens (e.g. looking in ~/.xsession-errors)
CCBUG: 213895

------------------------------------------------------------------------
r1184990 | scripty | 2010-10-12 16:19:42 +1300 (Tue, 12 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1185146 | mart | 2010-10-13 00:02:51 +1300 (Wed, 13 Oct 2010) | 2 lines

backport fix of extendergroup collapsing

------------------------------------------------------------------------
r1185518 | bholst | 2010-10-14 04:14:48 +1300 (Thu, 14 Oct 2010) | 9 lines

Adding missing beginInsertRows() and endInsertRows() calls in KPageWidgetModel:
beginInsertRows() has to be called before inserting rows
and endInsertRows() has to be called after inserting rows in a subclass of
QAbstractItemModel.

CCBUG: 249706

Backport of r1185487

------------------------------------------------------------------------
r1185804 | mlaurent | 2010-10-14 22:18:59 +1300 (Thu, 14 Oct 2010) | 2 lines

Backport: fix mem leak

------------------------------------------------------------------------
r1186278 | dfaure | 2010-10-16 05:00:27 +1300 (Sat, 16 Oct 2010) | 3 lines

Also assume that the application is a KDE application when its desktop file says Type=Service rather than Type=Application.
This fixes launching khelpcenter using Help / Foo Handbook (F1), which would use kioexec for "downloading" the help:/ url.

------------------------------------------------------------------------
r1186358 | cfeck | 2010-10-16 13:19:31 +1300 (Sat, 16 Oct 2010) | 5 lines

Use correct colorGroup for item text (backport r1186357)

BUG: 253022
FIXED-IN: 4.5.3

------------------------------------------------------------------------
r1186448 | mart | 2010-10-17 01:36:29 +1300 (Sun, 17 Oct 2010) | 2 lines

backport crash fix on second drag

------------------------------------------------------------------------
r1186452 | mart | 2010-10-17 01:50:40 +1300 (Sun, 17 Oct 2010) | 2 lines

backport spacerWidget removal from dragLeaveEvent

------------------------------------------------------------------------
r1186854 | pletourn | 2010-10-18 07:51:55 +1300 (Mon, 18 Oct 2010) | 4 lines

Don't loop forever

BUG:254048

------------------------------------------------------------------------
r1186862 | pletourn | 2010-10-18 08:32:29 +1300 (Mon, 18 Oct 2010) | 4 lines

Advance the cursor even when no case conversion was done

BUG:253309

------------------------------------------------------------------------
r1186917 | camuffo | 2010-10-18 10:57:48 +1300 (Mon, 18 Oct 2010) | 2 lines

backport r1186916, start always with a default size

------------------------------------------------------------------------
r1186956 | scripty | 2010-10-18 15:51:09 +1300 (Mon, 18 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1187160 | rkcosta | 2010-10-19 02:43:38 +1300 (Tue, 19 Oct 2010) | 9 lines

Backport r1186343, by dfaure.

Fix handling of large .zip files, the size determination ended up negative
because of the implicit sign expansion
when doing ... | (uchar)buffer[3] << 24   (int is implicitely used). Tested in
isolation, but not with .zip files.

CCBUG: 216672

------------------------------------------------------------------------
r1187161 | rkcosta | 2010-10-19 02:46:52 +1300 (Tue, 19 Oct 2010) | 8 lines

Backport r1187156.

Take qint64's instead of int's in KLimitedIODevice's constructor.

The class attributes were already qint64, and receiving int's made it fail on huge (2GB+) files.

CCBUG: 216672

------------------------------------------------------------------------
r1187508 | dfaure | 2010-10-20 05:39:52 +1300 (Wed, 20 Oct 2010) | 1 line

backport fix r1154548
------------------------------------------------------------------------
r1187766 | dfaure | 2010-10-21 00:51:55 +1300 (Thu, 21 Oct 2010) | 2 lines

Repair test with Qt-4.6.4 too. We have 3 cases in fact: no caching (ok), unreliable caching (skip test), fixed caching (>= 4.7.1, ok)

------------------------------------------------------------------------
r1187999 | mpyne | 2010-10-21 13:17:48 +1300 (Thu, 21 Oct 2010) | 17 lines

Backport a "valid page" check for KSharedDataCache to KDE 4.5.3.

This adds additional checks that seem to prevent crashes when changing the system time.
This only masks the symptom though, it should be a logic error to be removing a page
that is already marked as free in the code paths that could reach the affected function.
Either way, it's one less crash.

I'll leave the bug open until I feel I've figured out why the underlying cause of the
bug when changing system time.

As an aside to Beat Wolf, I am just as likely (if not more) to lose patches on
ReviewBoard as I am on bugs.kde.org.  Adding BugZilla attachments to
ReviewBoard isn't necessary for that reason, ReviewBoard should be for things
that require actual code review. ;)

CCBUG:253795

------------------------------------------------------------------------
r1188022 | pletourn | 2010-10-21 16:07:31 +1300 (Thu, 21 Oct 2010) | 4 lines

Let QScrollBar take care of the wheel event

BUG:239070

------------------------------------------------------------------------
r1188133 | skelly | 2010-10-22 02:36:17 +1300 (Fri, 22 Oct 2010) | 1 line

Use this hack until Qt 4.7.2
------------------------------------------------------------------------
r1188178 | ppenz | 2010-10-22 04:57:00 +1300 (Fri, 22 Oct 2010) | 6 lines

Revert the patch to handle ' ', '.' and '_' equally, as this violates the rule: if a < b and b < c, a must also be < c. The issue can be reproduced with the strings abc.jpg, abc1.jpg and abc_a.jpg. I really propose to make this "natural sorting" not more clever than necessary, just handling digits in a "natural" way is sufficient from my point of view.

BUG: 237788
FIXED-IN: 4.5.3


------------------------------------------------------------------------
r1188587 | dfaure | 2010-10-23 07:31:14 +1300 (Sat, 23 Oct 2010) | 6 lines

Fix mounting partitions as user, the "second step" wasn't done correctly (dev wasn't emptied, and the command line wasn't reset)
Of course I debugged it before seeing that bug 172023 had a patch for one of the two issues...
FIXED-IN: 4.5.3
BUG: 172023
(next step, fixing the icon, and fixing this logic again for bug 98804...)

------------------------------------------------------------------------
r1188632 | dfaure | 2010-10-23 08:17:28 +1300 (Sat, 23 Oct 2010) | 3 lines

Backport r1188630: Use "emblem-mounted" to show mounted devices, looks better than the old
UnmountIcon/Icon solution.

------------------------------------------------------------------------
r1188635 | dfaure | 2010-10-23 08:39:03 +1300 (Sat, 23 Oct 2010) | 3 lines

Remove unused "unmounted icon" button in the properties dialog.
CCBUG: 172023

------------------------------------------------------------------------
r1188641 | dfaure | 2010-10-23 09:04:26 +1300 (Sat, 23 Oct 2010) | 5 lines

Only show the "% of use" on a device when the device is actually mounted.
Otherwise we were showing the data for the parent device (e.g. "/", for unmounted "/mnt/foo"), which made no sense.

Also fix a huge negative number being shown for /proc (0/0)

------------------------------------------------------------------------
r1189096 | mwolff | 2010-10-24 13:58:52 +1300 (Sun, 24 Oct 2010) | 1 line

reenable ThreadWeaverJobTest (except three tests that cause freezes, I'll investigate)
------------------------------------------------------------------------
r1189097 | mwolff | 2010-10-24 13:58:54 +1300 (Sun, 24 Oct 2010) | 3 lines

don't crash when dequeing job sequeence in suspended weaver

BUG: 251976
------------------------------------------------------------------------
r1189110 | scripty | 2010-10-24 15:55:30 +1300 (Sun, 24 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1189303 | laidig | 2010-10-25 03:11:46 +1300 (Mon, 25 Oct 2010) | 4 lines

backport of 1189302: fix building the search index for "see also" references

CCBUG:214472

------------------------------------------------------------------------
r1189782 | pletourn | 2010-10-26 11:00:27 +1300 (Tue, 26 Oct 2010) | 4 lines

Obey the user

BUG:250367

------------------------------------------------------------------------
r1189784 | pletourn | 2010-10-26 11:11:03 +1300 (Tue, 26 Oct 2010) | 2 lines

Match '$' only with end of lines when searching backward

------------------------------------------------------------------------
r1189835 | pletourn | 2010-10-26 14:20:16 +1300 (Tue, 26 Oct 2010) | 2 lines

Fix for backward search in selection

------------------------------------------------------------------------
r1189841 | pletourn | 2010-10-26 14:32:02 +1300 (Tue, 26 Oct 2010) | 4 lines

<DetectSpaces /> was preventing the fallthru

BUG:255280

------------------------------------------------------------------------
r1189846 | pletourn | 2010-10-26 16:05:17 +1300 (Tue, 26 Oct 2010) | 3 lines

Revert last 2 commmits
Will solve bug another way

------------------------------------------------------------------------
r1190627 | mueller | 2010-10-29 00:43:02 +1300 (Fri, 29 Oct 2010) | 2 lines

version bump

------------------------------------------------------------------------
r1190831 | scripty | 2010-10-29 15:44:34 +1300 (Fri, 29 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
