2008-01-31 12:18 +0000 [r769069]  lunakl

	* branches/KDE/4.0/kdelibs/kdeui/windowmanagement/kwindowsystem_x11.cpp:
	  Fix accidental merging of two array fields into one.

2008-01-31 12:31 +0000 [r769072]  lunakl

	* branches/KDE/4.0/kdelibs/kdeui/windowmanagement/kwindowsystem_x11.cpp:
	  Make stackingOrder() work - INFO_BASIC is enough for it, and the
	  first call to updateStackingOrder() happened before activate()
	  and as such didn't work.

2008-01-31 16:55 +0000 [r769142]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/html/html_objectimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/html/html_objectimpl.h: Fix a
	  regression in <object><embed> handling.

2008-01-31 19:01 +0000 [r769176]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp: Instead of
	  trying to figure out whether to do a silent focus clear when a
	  previously-focus widget is getting destroyed in both the document
	  and the view (and getting them out-of-sync), have a special
	  quietResetFocus() method, and call it from the view's
	  focusNextPrevNode as appropriate. Fixes a crash when ignoring
	  requests on FaceBook BUG: 155434

2008-02-01 08:23 +0000 [r769330]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/actions/kstandardaction.h: can
	  also be connected to one widget's close slot, cf k-c-d

2008-02-01 09:34 +0000 [r769363]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/shortcuts/kglobalaccel.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/shortcuts/kglobalaccel_p.h,
	  branches/KDE/4.0/kdelibs/kdeui/shortcuts/kglobalaccel.h:
	  Re-register global shortcuts if kded4 is restarted. BUG: 156578

2008-02-01 14:37 +0000 [r769475]  mueller

	* branches/KDE/4.0/kdelibs/CMakeLists.txt: update version number a
	  little

2008-02-01 21:55 +0000 [r769717]  dfaure

	* branches/KDE/4.0/kdelibs/kparts/browserrun.cpp,
	  branches/KDE/4.0/kdelibs/khtml/khtml_run.cpp,
	  branches/KDE/4.0/kdelibs/kio/kio/krun.cpp,
	  branches/KDE/4.0/kdelibs/kio/kio/krun.h: Fix crash in K*Run due
	  to nested event loops (modal dialogs) deleting the KRun instance
	  from a timer (e.g. the redirection timer in KHTML, #137678)
	  Revert Aurélien's KHTMLRun fix for netvibes.com: the KRun fix
	  covers this case, and that KHTMLRun change introduced #156447.
	  Also deprecated a number of useless public methods in KRun
	  (Tobias added getters/setters for all previously-protected
	  variables, but some of them are really internal). I wish I had
	  reviewed this API before 4.0.0 :(

2008-02-02 13:20 +0000 [r769947]  mlaurent

	* branches/KDE/4.0/kdelibs/phonon/kcm/main.cpp: Disable help button
	  (there is not doc)

2008-02-02 16:20 +0000 [r770025]  dfaure

	* branches/KDE/4.0/kdelibs/kded/kded.cpp: Fix crash when lib==0
	  (happened to me with kded_networkstatus)

2008-02-02 18:07 +0000 [r770072]  mlaurent

	* branches/KDE/4.0/kdelibs/kio/kio/kurifilter.cpp: Backport: fix
	  debug

2008-02-02 20:25 +0000 [r770106]  mkretz

	* branches/KDE/4.0/kdelibs/kdeui/util/kcursor_p.h,
	  branches/KDE/4.0/kdelibs/kdeui/util/kcursor.cpp: backport SVN
	  commit 770102 by mkretz: don't keep dangling filter pointers in
	  the m_eventFilters hash for viewports

2008-02-02 21:18 +0000 [r770128]  brandybuck

	* branches/KDE/4.0/kdelibs/cmake/modules/FindKDE4Internal.cmake:
	  Place Qt includes before platform/X11 includes

2008-02-03 17:18 +0000 [r770410]  porten

	* branches/KDE/4.0/kdelibs/khtml/html/dtd.cpp: Merged revision
	  770284: Allow more violations of HTML 4 because these violations
	  are required for the Acid 3 test.

2008-02-03 20:08 +0000 [r770465]  lunakl

	* branches/KDE/4.0/kdelibs/kdeui/windowmanagement/kwindowsystem_x11.cpp:
	  Fix incorrect usage of setState(). Change NET::Sticky when
	  changing desktops with viewports. Since NETWinInfo::setState()
	  needs to read the state anyway, specify it explicitly to make it
	  a bit more efficient.

2008-02-03 20:15 +0000 [r770470]  lunakl

	* branches/KDE/4.0/kdelibs/kdeui/windowmanagement/kwindowsystem_x11.cpp:
	  Detect possible NET::Sticky changes.

2008-02-03 21:45 +0000 [r770515]  porten

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_html.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_window.cpp,
	  branches/KDE/4.0/kdelibs/kjs/object.h: Properly handle getter
	  properties on window and document object. Fixes a crash on:
	  window.__defineGetter__('foo', function() { return 33; }); var
	  bar = foo;

2008-02-04 12:25 +0000 [r770702]  mkretz

	* branches/KDE/4.0/kdelibs/phonon/videoplayer.h,
	  branches/KDE/4.0/kdelibs/phonon/videoplayer.cpp: backport SVN
	  commit 770700 by mkretz: This code is _very_ old (it's a leftover
	  from the first KDEMM prototype for KDE3) and I can't find a
	  reason why it's still needed. To the contrary I think it made
	  VideoPlayer behave different than described in the docs. (I.e. it
	  would start playing even if you called only load())

2008-02-04 15:18 +0000 [r770863]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/tests/kwindowtest.cpp: the
	  question on every startup is annoying (should be command-line
	  options I guess)

2008-02-04 15:22 +0000 [r770868]  lunakl

	* branches/KDE/4.0/kdelibs/kdecore/kernel/kautostart.cpp,
	  branches/KDE/4.0/kdelibs/kinit/autostart.cpp: Add xdg autostart
	  directories. Packagers: If there are any problematic autostart
	  entries there, you may override them in the KDE autostart
	  directory. CCMAIL: kde-packagers@kde.org

2008-02-05 12:00 +0000 [r771167]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/actions/kaction.h: docu is always
	  good to have

2008-02-05 12:38 +0000 [r771179-771178]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/tests/kxmlguitest.h: fix
	  overloaded virtual

	* branches/KDE/4.0/kdelibs/kdeui/tests/kglobalshortcuttest.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/actions/kaction.h: With Aurelien's
	  fix (kdedglobalaccel now also remembers actions with no default
	  shortcut and no active shortcut defined, for the kcm), we can't
	  make kdedglobalaccel forget a shortcut by simply calling it with
	  (empty,empty,NoAutoLoading). Remove this from the docs and from
	  the unit test; it doesn't seem that it was used anywhere else. If
	  we do need this again we'll have to add proper API for it, but I
	  doubt it.

2008-02-05 13:09 +0000 [r771188]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/tests/kglobalshortcuttest.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/tests/kglobalshortcuttest.h: Test
	  more things (including the fix for the bug where actions would be
	  missing in the kcm)

2008-02-05 13:42 +0000 [r771204]  rjarosz

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp: Backport commit
	  771201. Make sure that static widget is always at position 0,0.
	  CCBUG: 155820

2008-02-05 18:49 +0000 [r771329]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/shortcuts/kglobalaccel.cpp: Don't
	  trigger an action with a global shortcut if it's disabled.
	  Writing a unit test for that one seems very difficult (would need
	  XTest stuff on X11 etc.)

2008-02-05 20:32 +0000 [r771396]  qbast

	* branches/KDE/4.0/kdelibs/kfile/kfilewidget.cpp: Backport fixed
	  updating location bar for remote directories

2008-02-06 19:58 +0000 [r771719]  aacid

	* branches/KDE/4.0/kdelibs/kdeui/dialogs/kshortcutsdialog.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/widgets/kkeysequencewidget.cpp:
	  Backport r771716 **************** Use
	  QKeySequence::toString(QKeySequence::NativeText) for texts that
	  are user visible so we get the translated sequence instead the
	  english one

2008-02-07 09:07 +0000 [r771936]  dfaure

	* branches/KDE/4.0/kdelibs/khtml/java/kjavaappletserver.cpp: Use
	  KSSLCertificate::verifyText instead of duplicating the error
	  messages here.

2008-02-08 18:38 +0000 [r772428]  djarvie

	* branches/KDE/4.0/kdelibs/kdecore/date/kdatetime.cpp: Backport SVN
	  commit 772427: Prevent loss of cache data in values returned by
	  addSecs(0), etc. Fix compiler warnings about extern symbols.

2008-02-09 02:05 +0000 [r772562]  donga

	* branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/underFDL.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/catalog
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/underBSDLicense.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/report-bugs.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/strings.entities
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/underX11License.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/lgpl-notice.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/underGPL.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/underArtisticLicense.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/fdl-notice.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/update-doc.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/help-menu.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/install-compile.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/underLGPL.docbook
	  (added), branches/KDE/4.0/kdelibs/kdoctools/customization/th
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/user.entities
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/install-intro.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/entities/gpl-notice.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/th/lang.entities
	  (added): commit th entities

2008-02-10 08:50 +0000 [r773099]  donga

	* branches/KDE/4.0/kdelibs/kdoctools/customization/th/user.entities:
	  add user entities to th/user.entities

2008-02-10 18:00 +0000 [r773271]  mlaurent

	* branches/KDE/4.0/kdelibs/kutils/kcmultidialog.cpp,
	  branches/KDE/4.0/kdelibs/kutils/kcmultidialog_p.h: Backport:
	  Update button when we have just one page (fix disable/enable
	  button when we use kcmshell4 <module>)

2008-02-11 13:01 +0000 [r773578]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/widgets/kmainwindow.cpp: Fix
	  porting error spotted by ereslibre. Check this: KDE3: return new
	  KToolBar(this, DockTop, false, name, honor_mode ); // non-XMLGUI
	  KDE3: KToolBar( QMainWindow *parentWindow,
	  QMainWindow::ToolBarDock dock, bool newLine = false, const char
	  *name = 0, bool honorStyle = false, bool readConfig = true );
	  KDE4: new KToolBar(this, false, honor_mode ); KDE4: KToolBar(
	  QWidget *parent, bool honorStyle = false, bool readConfig = true
	  ); But this shouldn't affect many apps, since most use xmlgui.

2008-02-11 19:40 +0000 [r773799]  rjarosz

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp: Backport commit
	  773797. Don't clear contents offset. Fixes wrong scrollbar
	  position on reload and properly restores previous offset.

2008-02-12 10:37 +0000 [r774007]  dfaure

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_html.cpp: Fix crash on
	  http://msdn.microsoft.com/library/default.asp?url=/library/en-us/vccore/html/LNK4006.asp

2008-02-12 13:12 +0000 [r774075]  dfaure

	* branches/KDE/4.0/kdelibs/kio/kio/kdirmodel.cpp: Much faster
	  implementation of ChildCountRole. Peter tested it and said:
	  "Previously it took around 2 seconds until my test folders
	  appeared, now the folder is shown immediately (at least I can see
	  no performance difference in comparison with a folder having no
	  sub directories)." BUG: 157266

2008-02-12 19:04 +0000 [r774210]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/tests/kiconloader_unittest.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/icons/kiconloader.cpp: Fix
	  KIconLoader::loadIcon() with canReturnNull being false one time
	  and true next time, or vice-versa. The "last loaded icon" caching
	  didn't take that into account so the method misbehaved, and the
	  code would have gone wrong in release mode too, since it would
	  put the "unknown" icon into KIconCache and then
	  canReturnNull=true cannot work anymore (it would always return
	  the icon from the cache). CCMAIL: uwolfer@kde.org,
	  rivolaks@hot.ee

2008-02-12 19:59 +0000 [r774232]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/tests/kiconloader_unittest.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/icons/kicontheme.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/icons/kiconloader.cpp: My last
	  commit made the "No such icon" warning appear more than before
	  (since we don't cache unknown icons anymore); so ensure that end
	  users don't get that warning, they aren't going to draw icons
	  anyway. Also fix recurrent warning about invalid "Stock" context
	  as found on kubuntu systems at least.

2008-02-13 17:58 +0000 [r774630]  edulix

	* branches/KDE/4.0/kdelibs/kate/syntax/data/diff.xml: backport from
	  trunk: fixed collapsing of blocks in diff files

2008-02-13 19:44 +0000 [r774661]  dfaure

	* branches/KDE/4.0/kdelibs/kdecore/config/kconfiggroup.h: this arg
	  is gone

2008-02-13 22:37 +0000 [r774759]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/tests/klineedit_unittest.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/widgets/kcombobox.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/tests/klineedit_unittest.h,
	  branches/KDE/4.0/kdelibs/kdeui/widgets/kcombobox.h: Fix KComboBox
	  not emitting returnPressed when it was created with rw=false and
	  setEditable(true) afterwards. It really sucks that
	  QComboBox::setLineEdit() is not virtual anymore. BUG: 157785

2008-02-14 15:27 +0000 [r774985]  mutz

	* branches/KDE/4.0/kdelibs/kdeui/widgets/knuminput.cpp: setRange()
	  -> sliderEnabled(true)?? I beg your pardon??

2008-02-14 16:17 +0000 [r774998]  dfaure

	* branches/KDE/4.0/kdelibs/kparts/browserview.desktop: remove
	  special keys that were used for the old
	  toolbar-viewmode-switching in konqueror

2008-02-15 02:50 +0000 [r775151]  ilic

	* branches/KDE/4.0/kdelibs/kdoctools/customization/xsl/sr@latin.xml
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/underFDL.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/underLGPL.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/user.entities,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/install-intro.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/report-bugs.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/lang.entities,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/gpl-notice.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/strings.entities
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/lgpl-notice.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/catalog,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/underGPL.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/underBSDLicense.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/underX11License.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/underArtisticLicense.docbook
	  (added), branches/KDE/4.0/kdelibs/kdoctools/CMakeLists.txt,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/fdl-notice.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/underLGPL.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/update-doc.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/user.entities
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/install-intro.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/gpl-notice.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/lang.entities
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/entities/l10n.entities,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/help-menu.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/install-compile.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/catalog
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/dtd/rdbpool.elements,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/obsolete/rdbpool.elements,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/underBSDLicense.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/underX11License.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/xsl/sr.xml,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/underFDL.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/strings.entities,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/fdl-notice.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/lgpl-notice.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/update-doc.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/underGPL.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/help-menu.docbook
	  (added),
	  branches/KDE/4.0/kdelibs/kdoctools/customization/catalog,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr/entities/underArtisticLicense.docbook,
	  branches/KDE/4.0/kdelibs/kdoctools/customization/sr@latin/entities/install-compile.docbook
	  (added): Base files and additions for documentation in Serbian.
	  (bport: 775148)

2008-02-15 08:20 +0000 [r775218]  mutz

	* branches/KDE/4.0/kdelibs/kdeui/widgets/knuminput.h: Update apidox
	  after yesterday's fix

2008-02-15 15:43 +0000 [r775349]  dfaure

	* branches/KDE/4.0/kdelibs/KDE4PORTING.html: missing KProcess
	  entry, as noted by vincenzo and confirmed by ossi

2008-02-15 16:41 +0000 [r775367]  dfaure

	* branches/KDE/4.0/kdelibs/kio/kio/kdesktopfileactions.cpp: Avoid
	  nested event loop in DBUS call, made #149736 crash again.

2008-02-16 10:20 +0000 [r775575]  dimassa

	* branches/KDE/4.0/kdelibs/KDE4PORTING.html: typo fixed

2008-02-16 15:22 +0000 [r775754]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom2_traversalimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_traversal.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom2_traversalimpl.cpp: Merged
	  revision 775467: Return undefined when a node filter has an
	  exception. Also reset the node iterator when an exception is
	  thrown (unfortunately I dont see a nicer way to do this). This
	  fixes acid3 test 1.

2008-02-18 12:44 +0000 [r776576]  mutz

	* branches/KDE/4.0/kdelibs/kdecore/kdebug.areas: libkleo isn't in
	  kdepimlibs anymore. Add debug areas for kleopatra & friends.

2008-02-18 19:40 +0000 [r776767]  rjarosz

	* branches/KDE/4.0/kdelibs/kdeui/util/kwallet.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/util/kwallet.h: Backport commit
	  776766. Emit walletOpened signal if DBus error occurred otherwise
	  clients will be waiting forever for a wallet. Fixes wallet
	  problems in Kopete.

2008-02-18 20:48 +0000 [r776803]  dhaumann

	* branches/KDE/4.0/kdelibs/kate/syntax/data/php.xml: backport: php
	  keyword exception. (first test with svnbackport and 4.0 branch :)

2008-02-18 21:03 +0000 [r776816]  ereslibre

	* branches/KDE/4.0/kdelibs/kdeui/dialogs/kmessagebox.cpp: Make the
	  content label to do not draw any background. This will make the
	  dialog to be shown properly. Backport.

2008-02-18 21:23 +0000 [r776833]  ereslibre

	* branches/KDE/4.0/kdelibs/kdeui/dialogs/kmessagebox.cpp: Backport
	  of rev 776831

2008-02-18 21:45 +0000 [r776845]  dhaumann

	* branches/KDE/4.0/kdelibs/kate/utils/kateschema.cpp,
	  branches/KDE/4.0/kdelibs/kate/utils/kateconfig.cpp: backport: for
	  tab and trailing-space highlighting choose a dark color CCBUG:
	  157628

2008-02-19 17:49 +0000 [r777076]  ervin

	* branches/KDE/4.0/kdelibs/solid/solid/backends/hal/halvolume.cpp:
	  Whitelisting /mnt mountpoint as "not ignored". (backporting
	  r777075)

2008-02-19 18:53 +0000 [r777102]  uwolfer

	* branches/KDE/4.0/kdelibs/kdeui/widgets/klineedit.cpp: Backport:
	  SVN commit 777098 by uwolfer: Properly initialize the
	  QStyleOptionFrame. That's why this protected function is there in
	  QLineEdit. Fixes margin problems with Oxygen style and works also
	  fine with Plastique.

2008-02-19 22:10 +0000 [r777177]  aacid

	* branches/KDE/4.0/kdelibs/kdoctools/CMakeLists.txt: install th
	  customization too

2008-02-20 20:26 +0000 [r777511]  neundorf

	* branches/KDE/4.0/kdelibs/cmake/modules/FindKDE4Internal.cmake:
	  -add INSTALL_TARGETS_DEFAULT_ARGS so e.g. kdepim trunk can work
	  with kdelibs from the 4.0 branch while still being able to
	  install correctly also on windows -allow duplicated targets, used
	  e.g. for buildtests (for cmake 2.6 compatibility) -set
	  CMAKE_LINK_OLD_PATHS to TRUE, so make sure linking works even if
	  libs without full path are used (also cmake 2.6 compatiblity)
	  Alex

2008-02-20 21:59 +0000 [r777553]  neundorf

	* branches/KDE/4.0/kdelibs/cmake/modules/FindKDE4Internal.cmake:
	  -give useful error message if a cmake cvs without set_property()
	  is used Alex

2008-02-22 11:15 +0000 [r778058]  mutz

	* branches/KDE/4.0/kdelibs/kdecore/config/kconfiggroup.h: ++apidox

2008-02-22 18:23 +0000 [r778170]  mutz

	* branches/KDE/4.0/kdelibs/kdeui/paged/kpageview.cpp: Fix
	  regression from KDE3: KCM's with & in their comment are displayed
	  wrong. The removal of '&' is just wrong. '&' is not special when
	  the label doesn't have a buddy, and this one doesn't.

2008-02-22 19:58 +0000 [r778194]  lunakl

	* branches/KDE/4.0/kdelibs/kdeui/CMakeLists.txt,
	  branches/KDE/4.0/kdelibs/kdeui/util/kxutils.cpp (added),
	  branches/KDE/4.0/kdelibs/kdeui/windowmanagement/kwindowsystem_x11.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/util/kxutils.h (added): Function
	  for creating QPixmap from X Pixmap, and try to handle the case
	  when the depths differ. BUG: 153637

2008-02-23 11:39 +0000 [r778323]  ossi

	* branches/KDE/4.0/kdelibs/kdecore/config/kconfigini.cpp: (finally)
	  backport: SVN commit 764908 by braxton: This should fix the
	  problem with saving settings on app exit. The problem was I was
	  unintentionally creating extra references to the global
	  component-data by leaking the lockfile. This caused the global
	  component-data d-tor to never be called therefore the d-tor for
	  the global config-file was never called so it didn't sync on
	  exit.

2008-02-23 13:05 +0000 [r778340]  mlaurent

	* branches/KDE/4.0/kdelibs/kate/utils/katesearchbar.cpp: Backport:
	  Allow to use enter to replace string

2008-02-23 22:37 +0000 [r778570]  ilic

	* branches/KDE/4.0/kdelibs/kdeui/fonts/kfontchooser.cpp: Backport
	  778569.

2008-02-24 10:37 +0000 [r778689]  lunakl

	* branches/KDE/4.0/kdelibs/kdeui/util/kxerrorhandler.cpp: Backport,
	  disable code that can potentially deadlock.

2008-02-24 16:25 +0000 [r778788]  ilic

	* branches/KDE/4.0/kdelibs/kdoctools/docbook/xsl/common/sr.xml,
	  branches/KDE/4.0/kdelibs/kdoctools/docbook/xsl/common/sr_latin.xml:
	  Latest XSL definitions for Serbian. (bport: 778787)

2008-02-25 21:55 +0000 [r779324]  mrybczyn

	* branches/KDE/4.0/kdelibs/kdoctools/customization/pl/user.entities:
	  Added entities to correctly generate documentation.

2008-02-26 10:40 +0000 [r779512]  mueller

	* branches/KDE/4.0/kdelibs/CMakeLists.txt: starting 4.0.2

2008-02-26 17:08 +0000 [r779621]  mutz

	* branches/KDE/4.0/kdelibs/kdeui/widgets/ktabwidget.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/widgets/ktabbar.cpp: Fix
	  KTab{Widget,Bar}'s egoistic DnD behaviour. Patch as posted on
	  k-c-d

2008-02-27 10:44 +0000 [r779901]  porten

	* branches/KDE/4.0/kdelibs/kjs/JSImmediate.h: Added include
	  required for signbit()

2008-02-28 09:22 +0000 [r780189]  dfaure

	* branches/KDE/4.0/kdelibs/kded/test/kmimefileparsertest.h
	  (removed),
	  branches/KDE/4.0/kdelibs/kdecore/services/kservice.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/services/kservice.h,
	  branches/KDE/4.0/kdelibs/kdecore/services/kservicetypeprofile_p.h,
	  branches/KDE/4.0/kdelibs/kdecore/tests/kmimetypetest.cpp,
	  branches/KDE/4.0/kdelibs/kded/test/CMakeLists.txt,
	  branches/KDE/4.0/kdelibs/kded/kbuildsycoca.cpp,
	  branches/KDE/4.0/kdelibs/kded/kmimeassociations.cpp (added),
	  branches/KDE/4.0/kdelibs/kded/kbuildservicefactory.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/services/kservicetypeprofile.cpp,
	  branches/KDE/4.0/kdelibs/kded/kmimeassociations.h (added),
	  branches/KDE/4.0/kdelibs/kdecore/sycoca/ksycoca.h,
	  branches/KDE/4.0/kdelibs/kdecore/services/kservice_p.h,
	  branches/KDE/4.0/kdelibs/kded/kbuildservicefactory.h,
	  branches/KDE/4.0/kdelibs/kdecore/services/kservicetypeprofile.h,
	  branches/KDE/4.0/kdelibs/kdecore/services/kservicefactory.cpp,
	  branches/KDE/4.0/kdelibs/kded/CMakeLists.txt,
	  branches/KDE/4.0/kdelibs/kded/test/kmimefileparsertest.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/services/kmimetypetrader.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/services/kservicefactory.h,
	  branches/KDE/4.0/kdelibs/kded/test/kmimeassociationstest.cpp
	  (added): Backporting r779354+779356 Support for XDG spec
	  mimeapps.list instead of old ugly profilerc file, for configuring
	  the mime/app associations.

