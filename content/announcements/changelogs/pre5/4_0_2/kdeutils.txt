2008-02-03 20:14 +0000 [r770469]  mlaurent

	* branches/KDE/4.0/kdeutils/kdf/stdoption.cpp: Backport: We use
	  dolphin now

2008-02-03 20:23 +0000 [r770479]  mlaurent

	* branches/KDE/4.0/kdeutils/kdf/kwikdisk.cpp: Backport: Remove
	  duplicate "quit" entry

2008-02-03 23:22 +0000 [r770545]  brandybuck

	* branches/KDE/4.0/kdeutils/kcalc/CMakeLists.txt,
	  branches/KDE/4.0/kdeutils/kcalc/pics (removed),
	  branches/KDE/4.0/kdeutils/kcalc/kcalc.cpp: BUG: 155275 Follow
	  color scheme by using rich text for square root button

2008-02-04 19:20 +0000 [r770931]  hugopl

	* branches/KDE/4.0/kdeutils/kgpg/keysmanager.cpp: backport: Fix the
	  wrong signal name of a KSelectAction.

2008-02-06 16:51 +0000 [r771660]  lukas

	* branches/KDE/4.0/kdeutils/kgpg/kgpgoptions.cpp: missing icons

2008-02-11 22:48 +0000 [r773876]  aacid

	* branches/KDE/4.0/kdeutils/kdf/disklist.cpp: backport r773873
	  **************** don't change our env vars, just the spawned
	  process ones, fixes translations not working

2008-02-16 20:34 +0000 [r775859]  wirr

	* branches/KDE/4.0/kdeutils/superkaramba/src/themelocale.cpp: Fix a
	  crash when items in the languageList are removed

