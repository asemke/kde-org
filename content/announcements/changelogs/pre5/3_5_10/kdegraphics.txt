2008-02-18 19:22 +0000 [r776758]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Lexer.cc: xref can be
	  null so check it is not

2008-03-01 10:53 +0000 [r780844]  pino

	* branches/KDE/3.5/kdegraphics/kdvi/fontpool.cpp: Manually clear
	  the fonts before FreeType gets unloaded. Otherwise unloading the
	  FreeType font later can (and will) result in a crash. BUG: 105297
	  CCMAIL: kde-packager@kde.org

2008-03-11 20:13 +0000 [r784589]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/Splash.cc: Make
	  sure we don't draw outside the bitmap on Splash::fillGlyph2 I did
	  this patch on poppler and forgot to bring it here :-(

2008-03-14 19:34 +0000 [r785709]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/Splash.cc: Fix
	  "Make sure we don't draw outside the bitmap on
	  Splash::fillGlyph2" when painting with no aa Not useful for kpdf
	  as we always draw with aa but why have wrong core when we can
	  have the correct one?

2008-03-14 19:38 +0000 [r785712]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/Splash.cc: unbreak
	  my last commit :-(

2008-03-14 19:47 +0000 [r785716]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashXPathScanner.cc:
	  xx0 is set as maximum to buffer width so we need < not <= here
	  Fixes some crashes due to write out of bounds BUGS: 158387,
	  158549

2008-06-21 14:32 +0000 [r822788]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JBIG2Stream.cc:
	  somehow i forgot to commit this part of
	  http://cgit.freedesktop.org/poppler/poppler/commit/?id=2a12409ebbf96ea3ca4556b71231a45ae37cb052
	  Sorry :-( BUG: 164568

2008-06-24 13:01 +0000 [r823888]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/shell/kpdf.desktop: be more
	  compliant: - remove deprecated Encoding key - remove deprecated
	  %m from Exec key - fixup category PDFViewer -> Viewer

2008-06-29 22:51 +0000 [r826179]  anagl

	* branches/KDE/3.5/kdegraphics/kfile-plugins/rgb/kfile_rgb.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/dds/kfile_dds.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/ico/kfile_ico.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/ps/gsthumbnail.desktop,
	  branches/KDE/3.5/kdegraphics/kfaxview/kfaxview.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/tga/kfile_tga.desktop,
	  branches/KDE/3.5/kdegraphics/kview/modules/browser/kviewbrowser.desktop,
	  branches/KDE/3.5/kdegraphics/kghostview/kghostview_part.desktop,
	  branches/KDE/3.5/kdegraphics/kview/kview.desktop,
	  branches/KDE/3.5/kdegraphics/ksvg/plugin/svgthumbnail.desktop,
	  branches/KDE/3.5/kdegraphics/kview/modules/presenter/config/kviewpresenterconfig.desktop,
	  branches/KDE/3.5/kdegraphics/kruler/uninstall.desktop,
	  branches/KDE/3.5/kdegraphics/kpovmodeler/kpovmodeler.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/bmp/kfile_bmp.desktop,
	  branches/KDE/3.5/kdegraphics/kview/kviewcanvas/config/kviewcanvasconfig.desktop,
	  branches/KDE/3.5/kdegraphics/kghostview/kghostview.desktop,
	  branches/KDE/3.5/kdegraphics/kview/modules/scanner/kviewscanner.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/dvi/kfile_dvi.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/ps/kfile_ps.desktop,
	  branches/KDE/3.5/kdegraphics/kview/config/plugins/kviewpluginsconfig.desktop,
	  branches/KDE/3.5/kdegraphics/kview/modules/template/kviewtemplate.desktop,
	  branches/KDE/3.5/kdegraphics/kamera/kcontrol/kamera.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/png/kfile_png.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/xbm/kfile_xbm.desktop,
	  branches/KDE/3.5/kdegraphics/ksvg/plugin/ksvgplugin.desktop,
	  branches/KDE/3.5/kdegraphics/kview/config/kviewgeneralconfig.desktop,
	  branches/KDE/3.5/kdegraphics/ksvg/core/ksvgrenderer.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/tiff/kfile_tiff.desktop,
	  branches/KDE/3.5/kdegraphics/ksvg/plugin/backends/agg/ksvgaggcanvas.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/raw/kfile_raw.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/pcx/kfile_pcx.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/pnm/kfile_pnm.desktop,
	  branches/KDE/3.5/kdegraphics/kcoloredit/uninstall.desktop,
	  branches/KDE/3.5/kdegraphics/kooka/kooka.desktop,
	  branches/KDE/3.5/kdegraphics/kruler/kruler.desktop,
	  branches/KDE/3.5/kdegraphics/kcoloredit/kcolorchooser.desktop,
	  branches/KDE/3.5/kdegraphics/kmrml/kmrml/kcontrol/kcmkmrml.desktop,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaint.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/exr/kfile_exr.desktop,
	  branches/KDE/3.5/kdegraphics/kview/modules/presenter/kviewpresenter.desktop,
	  branches/KDE/3.5/kdegraphics/kiconedit/kiconedit.desktop,
	  branches/KDE/3.5/kdegraphics/kgamma/kcmkgamma/kgamma.desktop,
	  branches/KDE/3.5/kdegraphics/kfax/kfax.desktop,
	  branches/KDE/3.5/kdegraphics/ksnapshot/uninstall.desktop,
	  branches/KDE/3.5/kdegraphics/ksnapshot/ksnapshot.desktop,
	  branches/KDE/3.5/kdegraphics/kfaxview/kfaxmultipage.desktop,
	  branches/KDE/3.5/kdegraphics/kviewshell/plugins/djvu/djvumultipage.desktop,
	  branches/KDE/3.5/kdegraphics/kdvi/kdvi.desktop,
	  branches/KDE/3.5/kdegraphics/libkscan/scanservice.desktop,
	  branches/KDE/3.5/kdegraphics/kmrml/kmrml/server/daemonwatcher.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/xpm/kfile_xpm.desktop,
	  branches/KDE/3.5/kdegraphics/kview/photobook/photobook.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/jpeg/kfile_jpeg.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/gif/kfile_gif.desktop,
	  branches/KDE/3.5/kdegraphics/kfaxview/kfaxmultipage_tiff.desktop,
	  branches/KDE/3.5/kdegraphics/kpdf/kpdf_part.desktop,
	  branches/KDE/3.5/kdegraphics/kcoloredit/kcoloredit.desktop,
	  branches/KDE/3.5/kdegraphics/kview/kviewviewer/config/kviewviewerpluginsconfig.desktop,
	  branches/KDE/3.5/kdegraphics/kview/modules/effects/kvieweffects.desktop,
	  branches/KDE/3.5/kdegraphics/ksvg/plugin/backends/libart/ksvglibartcanvas.desktop,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.desktop,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/pdf/kfile_pdf.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-07-02 00:08 +0000 [r826998]  anagl

	* branches/KDE/3.5/kdegraphics/kamera/kcontrol/kamera.desktop,
	  branches/KDE/3.5/kdegraphics/kmrml/kmrml/kcontrol/kcmkmrml.desktop,
	  branches/KDE/3.5/kdegraphics/kpovmodeler/kpovmodeler.desktop,
	  branches/KDE/3.5/kdegraphics/kiconedit/kiconedit.desktop: Desktop
	  validation fixes: -remove deprecated entries SwallowExec and
	  SwallowTitle. -fix Categories.

2008-07-22 19:46 +0000 [r836688]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.h,
	  branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  Some documents have loops in XObject dictionaries, make sure we
	  don't get in an infinite loop while traversing them BUG: 166145

2008-08-17 10:04 +0000 [r848193]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/main.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/VERSION: kde 3.5.10 is coming

2008-08-19 19:47 +0000 [r849599]  coolo

	* branches/KDE/3.5/kdegraphics/kdegraphics.lsm: update for 3.5.10

