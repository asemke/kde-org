2008-01-05 20:32 +0000 [r757757]  dimsuz

	* branches/KDE/4.0/kdegames/kreversi/mainwindow.cpp,
	  branches/KDE/4.0/kdegames/kreversi/kreversiscene.h,
	  branches/KDE/4.0/kdegames/kreversi/kreversigame.cpp,
	  branches/KDE/4.0/kdegames/kreversi/kreversiscene.cpp,
	  branches/KDE/4.0/kdegames/kreversi/kreversigame.h,
	  branches/KDE/4.0/kdegames/kreversi/Engine.h: Backport a bugfix
	  from trunk (-r757647:757648): Do not crash when pressing "New
	  Game" while thinking

2008-01-06 21:31 +0000 [r758088]  aacid

	* branches/KDE/4.0/kdegames/kolf/scoreboard.cpp,
	  branches/KDE/4.0/kdegames/kolf/scoreboard.h: backport r758055
	  **************** better sizing for the scoreboard

2008-01-09 19:25 +0000 [r758991]  reitelbach

	* branches/KDE/4.0/kdebase/apps/doc/dolphin/index.docbook,
	  branches/KDE/4.0/kdebase/workspace/solid/solidshell/main.cpp,
	  branches/KDE/4.0/kdegraphics/kolourpaint/pixmapfx/kpPixmapFX_ScreenDepth.cpp,
	  branches/KDE/4.0/kdenetwork/kget/transfer-plugins/kio/transferKio.cpp,
	  branches/KDE/4.0/kdegames/kblackbox/main.cpp: some collected i18n
	  typo fixes that did not make it into KDE 4.0 due to the message
	  freeze.

2008-01-12 20:54 +0000 [r760510]  mlaurent

	* branches/KDE/4.0/kdegames/kfourinline/src/kwin4.cpp,
	  branches/KDE/4.0/kdegames/klines/klines.cpp,
	  branches/KDE/4.0/kdegames/kmahjongg/kmahjongg.cpp,
	  branches/KDE/4.0/kdegames/kblackbox/kbbmainwindow.cpp,
	  branches/KDE/4.0/kdegames/kiriki/src/kiriki.cpp,
	  branches/KDE/4.0/kdegames/kmahjongg/kmahjongg.desktop,
	  branches/KDE/4.0/kdegames/kbounce/mainwindow.cpp,
	  branches/KDE/4.0/kdegames/kmines/mainwindow.cpp,
	  branches/KDE/4.0/kdegames/kspaceduel/dialogs.cpp,
	  branches/KDE/4.0/kdegames/kshisen/app.cpp,
	  branches/KDE/4.0/kdegames/ksudoku/src/gui/ksudoku.cpp: Backport:
	  Fix help button

2008-01-16 17:42 +0000 [r762262]  woebbe

	* branches/KDE/4.0/kdegames/kshisen/app.cpp: move setupGUI() to the
	  right place to restore the window's size again

2008-01-16 22:13 +0000 [r762383]  aacid

	* branches/KDE/4.0/kdegames/ktuberling/pics/default_theme.theme:
	  these are earrings not ears!!!

2008-01-17 20:14 +0000 [r762753]  dimsuz

	* branches/KDE/4.0/kdegames/klines/previewitem.h,
	  branches/KDE/4.0/kdegames/klines/scene.cpp,
	  branches/KDE/4.0/kdegames/klines/previewitem.cpp: Backport of
	  bugfix: #153907

2008-01-18 21:38 +0000 [r763204]  aacid

	* branches/KDE/4.0/kdegames/ktuberling/soundfactory.cpp: make
	  speech switching work

2008-01-19 22:11 +0000 [r763609]  ducroquet

	* branches/KDE/4.0/kdegames/konquest/mapscene.cc,
	  branches/KDE/4.0/kdegames/konquest/gameview.cc: Backport a small
	  trunk change : remove the weird border around the game area

2008-01-20 15:55 +0000 [r763923]  ducroquet

	* branches/KDE/4.0/kdegames/konquest/mainwin.cc: Backport a small
	  trunk change : fix the toolbar and menu icons

2008-01-20 16:32 +0000 [r763935]  montanaro

	* branches/KDE/4.0/kdegames/libkdegames/carddecks/svg-tigullio-international/tigullio-international.svg:
	  Backport from head. Make it a valid SVG file again, fix Qt4.3
	  rendering issue.

2008-01-22 00:08 +0000 [r764526]  cartman

	* branches/KDE/4.0/kdegames/kbattleship/src/networkdialog.cpp:
	  merge 764524

2008-01-24 22:56 +0000 [r765940]  aacid

	* branches/KDE/4.0/kdegames/ktuberling/toplevel.cpp: backport
	  r765939 **************** ignore toggling of "nonchecked" actions
	  Fixes problem when loading a saved file

2008-01-28 10:49 +0000 [r767561]  montanaro

	* branches/KDE/4.0/kdegames/kgoldrunner/src/kgrdialog.cpp: Backport
	  dialog size calculation fix (on shallow-screen monitors, the
	  bottom of the dialog ended up outside of the screen). CMAIL: Ian
	  Wadham <ianw2@optusnet.com.au>

2008-01-28 21:06 +0000 [r767821]  aacid

	* branches/KDE/4.0/kdegames/ktuberling/main.cpp: increase version

