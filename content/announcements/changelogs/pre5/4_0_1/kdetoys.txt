2008-01-05 23:58 +0000 [r757814]  woebbe

	* branches/KDE/4.0/kdetoys/kworldclock/maploader.h,
	  branches/KDE/4.0/kdetoys/kworldclock/maploader.cpp: fix daylight
	  drawing

2008-01-06 00:18 +0000 [r757817]  woebbe

	* branches/KDE/4.0/kdetoys/kworldclock/mapwidget.cpp:
	  QAction::isChecked() already returns the new value

2008-01-06 00:36 +0000 [r757823]  woebbe

	* branches/KDE/4.0/kdetoys/kworldclock/mapwidget.cpp: toggleXXX()
	  are public slots, so it's better to invert something or make the
	  slots private and remove the bools, but not for 4.0.0

2008-01-08 17:15 +0000 [r758653]  mueller

	* branches/KDE/4.0/kdetoys/amor/main.cpp: fix crash

