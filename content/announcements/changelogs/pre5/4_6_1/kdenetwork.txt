------------------------------------------------------------------------
r1216369 | rkcosta | 2011-01-23 10:15:24 +1300 (Sun, 23 Jan 2011) | 24 lines

Backport r1216368.

Fix a bit sending several outgoing webcam requests + a typo fix that denies in some cases sending webcam data.

From the ReviewBoard description:

  a) seems like yahoo protocol doesn't send back in the webcam
  invitation accepted which person did that. and there is a variable
  to remember who we sent the last invite to. Problem would be if
  several invites are going to be sent. So, I made the invitation name
  to be an invitation *list*. Inspired by code in libpurple.

  b) there is a typo when code tries to guess who the 'accepted
  invitation' is coming from. I don't have a good way to replicate,
  but mostly I suspect it happens when you're sending several
  invitations (see point a) ) or when some online<->offline switches
  are happening.  Concrete side effect is that code tries to connect
  to server "".

Patch by Cristi P <cristi.posoiu AT gmail>, thanks a lot!

REVIEW: 6333


------------------------------------------------------------------------
r1216724 | scripty | 2011-01-25 01:19:45 +1300 (Tue, 25 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1217464 | scripty | 2011-01-28 01:03:59 +1300 (Fri, 28 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1217762 | scripty | 2011-01-29 02:05:35 +1300 (Sat, 29 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1217895 | rkcosta | 2011-01-30 02:08:15 +1300 (Sun, 30 Jan 2011) | 22 lines

Use libjasper instead of relying on the external jasper program.

From the original ReviewBoard description:

  I saw that for yahoo - webcam images are converted by running an
  external program. Personally I am not ok with this since running an
  external program every 0.X seconds is a bit expensive. Also, not to
  mention using disk files (at least pushing directly into the program
  and reading its output would have been better).

  Also - I saw that inviteWebcam action was checking for presence of
  jasper program - but requestWebcam should also have done that. Not
  to mention that if a yahoo contact just invites you to see his cam,
  you might get a feedback of missing program after accepting to
  invitation. Not necessarily a big deal, though.

Original patch by Cristi Posoiu <cristi.posoiu AT gmail>, thanks!

REVIEW: 6312
BUG: 244135


------------------------------------------------------------------------
r1217959 | rkcosta | 2011-01-30 13:28:28 +1300 (Sun, 30 Jan 2011) | 34 lines

yahoo protocol: Disconnect signals when login fails.

From the original ReviewBoard description:

  Basically, sometimes, kopete is displaying in duplicate incoming
  messages.  Checking my log, I saw the following:

  a) yahoo wasn't actually sending the message in duplicate.
  b) but YahooACcount::slotGotIm() was called twice on that message
  c) somewhere before the problematic thing - and I actually saw many
  things being done in duplicate (not just messages), I saw this:

    kopete(8352)/kopete (yahoo - raw protocol)
    Client::slotLoginResponse: Emitting loggedIn

    kopete(8352)/kopete (yahoo) YahooAccount::slotLoginResponse: -1 ,
    "" )]

  d) checking code in yahooaccount.cpp - it seems that for that code
  (and others), it considers that account is offline, but forgets to
  also disconnect all the signals. So, next time user logins (even by
  an auto-reconnect), the signals are connected *again* to the same
  sources. And that's why it gets to do things 2 times.

This commit contains the disconnection code without the message box
part, which includes new strings and will be committed only to trunk.

Original patch by Cristi Posoiu <cristi.posoiu AT gmail>, thanks!

REVIEW: 6421
BUG: 206159
FIXED-IN: 4.6.1


------------------------------------------------------------------------
r1218332 | murrant | 2011-02-02 16:23:34 +1300 (Wed, 02 Feb 2011) | 5 lines

Backport r1218329 by murrant from trunk to the 4.6 branch:

Clear the input field when the new connection action is fired, but not when a tab is closed.


------------------------------------------------------------------------
r1218772 | scripty | 2011-02-05 00:31:42 +1300 (Sat, 05 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1218790 | mfuchs | 2011-02-05 06:17:53 +1300 (Sat, 05 Feb 2011) | 2 lines

Show warning if a file exists already.
BUG:265153
------------------------------------------------------------------------
r1218793 | mfuchs | 2011-02-05 06:33:24 +1300 (Sat, 05 Feb 2011) | 3 lines

Backport r1218792
Extracts messages for konqueror extension to make it translatable.
CCBUG:265140
------------------------------------------------------------------------
r1218795 | mfuchs | 2011-02-05 07:14:44 +1300 (Sat, 05 Feb 2011) | 3 lines

When DropTarget is destructed the config is always written.
By default the drop target will appear in the middle of the screen.
BUG:265185
------------------------------------------------------------------------
r1218809 | mfuchs | 2011-02-05 08:27:26 +1300 (Sat, 05 Feb 2011) | 3 lines

Backport r1218805
Drop Target settings are disabled, if the Drop Target is not used.
Backport r1218805
------------------------------------------------------------------------
r1218810 | mfuchs | 2011-02-05 08:27:28 +1300 (Sat, 05 Feb 2011) | 3 lines

Backport r1218806
Do not overwrite the suggested file name when changing the destination.
CCBUG:264823
------------------------------------------------------------------------
r1218811 | mfuchs | 2011-02-05 08:27:31 +1300 (Sat, 05 Feb 2011) | 2 lines

Backport r1218807
Uses the default folder, if a file name has been specified but no folder.
------------------------------------------------------------------------
r1218827 | mfuchs | 2011-02-05 10:04:29 +1300 (Sat, 05 Feb 2011) | 1 line

Fix compilation error.
------------------------------------------------------------------------
r1218842 | mfuchs | 2011-02-05 11:37:52 +1300 (Sat, 05 Feb 2011) | 3 lines

Backport r1218840
Stopping jobs via the scheduler works again.
CCBUG:262098
------------------------------------------------------------------------
r1218860 | lvsouza | 2011-02-05 14:45:31 +1300 (Sat, 05 Feb 2011) | 7 lines

Backport r1218856 by lvsouza from trunk to the 4.6 branch:

Sorts accounts before adding them to the status bar.
Implementation relys on allPluginsLoaded working as expected.
I have been using this change for a week, no problems so far.


------------------------------------------------------------------------
r1219007 | lvsouza | 2011-02-06 05:30:25 +1300 (Sun, 06 Feb 2011) | 4 lines

Backport r1219006 by lvsouza from trunk to the 4.6 branch:

Making them static as suggested by Raphael Kubo da Costa.

------------------------------------------------------------------------
r1219173 | mfuchs | 2011-02-07 08:36:33 +1300 (Mon, 07 Feb 2011) | 6 lines

Backport r1219170
Segment retries to write data after some time.

If the Segment can't write its data it will wait for 50 msec and then retry.
If this fails 100 times an error is emitted.
CCBUG:248176
------------------------------------------------------------------------
r1219462 | lvsouza | 2011-02-09 09:33:44 +1300 (Wed, 09 Feb 2011) | 7 lines

Backport r1219459 by lvsouza from trunk to the 4.6 branch:

Make Oscar/Icq use Kopete's events instead of systems'
for error notification.

CCBUG: 265811

------------------------------------------------------------------------
r1219466 | mfuchs | 2011-02-09 09:38:08 +1300 (Wed, 09 Feb 2011) | 2 lines

Backport r1219465
File name suggestions by konqueror are not percent encoded anymore.
------------------------------------------------------------------------
r1219470 | mfuchs | 2011-02-09 10:12:10 +1300 (Wed, 09 Feb 2011) | 9 lines

Backport r1219468
Avoids a dangling pointer.

When doKill is called on KGetJobAdapter or KGetGlobalJob requestStop(KJob*, TransferHandler*) is emitted.
KUiServerJob reacts on that:
*unregisters the job if it was registered
*stops the transfer or all transfers if transfer is 0.
That way the job is unregistered before its KWidgetJobTracker is destroyed avoiding a crash.
CCBUG:265681
------------------------------------------------------------------------
r1219492 | lvsouza | 2011-02-09 15:36:27 +1300 (Wed, 09 Feb 2011) | 11 lines

Backport r1219491 by lvsouza from trunk to the 4.6 branch:

Uses user-offline.png as icon overlay to make it more
visible when user is offline.

OBS: I do not like using only color/grayscale icons
to indicate online/offline status, specially when
the icons are small and look very alike.

CCBUG: 256667

------------------------------------------------------------------------
r1219497 | lvsouza | 2011-02-09 16:48:36 +1300 (Wed, 09 Feb 2011) | 6 lines

Backport r1219496. by lvsouza from trunk to the 4.6 branch:

Hide InfoEventIconLabel if server messages reaches zero.

CCBUG: 263181

------------------------------------------------------------------------
r1219499 | lvsouza | 2011-02-09 17:04:09 +1300 (Wed, 09 Feb 2011) | 6 lines

Backport r1219498 by lvsouza from trunk to the 4.6 branch:

Enable contact-gone-offline custom notification.

CCBUG: 260892

------------------------------------------------------------------------
r1219502 | lvsouza | 2011-02-09 18:13:00 +1300 (Wed, 09 Feb 2011) | 7 lines

Backport r1219501 by lvsouza from trunk to the 4.6 branch:

Applying patch from  Christian Muehlhaeuser to force all accounts to
go online if the Status -> Online menu is clicked. Thanks for the patch.

CCBUG: 234000

------------------------------------------------------------------------
r1220225 | mfuchs | 2011-02-13 23:29:35 +1300 (Sun, 13 Feb 2011) | 2 lines

When closing a KWidgetJobTracker of a global job all transfers, not only the ones currently running, are stopped.
That way transfers with a Run policy won't be started.
------------------------------------------------------------------------
r1220228 | mfuchs | 2011-02-13 23:42:38 +1300 (Sun, 13 Feb 2011) | 2 lines

Invalidated transfers are not counted as existing running transfers.
This way removing the last running transfer with exportGlobalJob will also remove the global job.
------------------------------------------------------------------------
r1220899 | scripty | 2011-02-16 03:44:07 +1300 (Wed, 16 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1221357 | mblumenstingl | 2011-02-18 14:55:01 +1300 (Fri, 18 Feb 2011) | 5 lines

Backport of 1221356.
Fix "last seen" property when an account goes offline.

CCBUG: 266580

------------------------------------------------------------------------
r1221522 | lvsouza | 2011-02-19 06:16:09 +1300 (Sat, 19 Feb 2011) | 9 lines

Backport r1221521 by lvsouza from trunk to the 4.6 branch:

Show contact tooltips even when main window has not focus.
This problem happens when using "click to focus" window policy.
It does not happen when using "focus follow mouse" policy.
Thanks Adria for the patch.

CCBUG: 266559

------------------------------------------------------------------------
r1221605 | scripty | 2011-02-20 02:35:45 +1300 (Sun, 20 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1221798 | scripty | 2011-02-21 02:15:08 +1300 (Mon, 21 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1222555 | mfuchs | 2011-02-25 10:31:18 +1300 (Fri, 25 Feb 2011) | 5 lines

Revert r1218842
Scheduler won't stop jobs automatically.
Stopping them automatically resulted in problems when downloading multiple files from the same host.
Whenever too many connections existed it could happen that transfers would not complete as they were stopped by the scheduler.
This will stay that way, until there is handling of a connection limit to hosts.
------------------------------------------------------------------------
