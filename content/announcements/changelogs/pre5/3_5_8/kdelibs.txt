2007-05-15 05:48 +0000 [r664896]  carewolf

	* branches/KDE/3.5/kdelibs/khtml/rendering/render_flow.cpp: Fix
	  insert inline in inline in block-context in inline bug. CCBUG:
	  145428

2007-05-15 13:10 +0000 [r664989]  mueller

	* branches/KDE/3.5/kdelibs/kio/kio/kprotocolinfo.h: apply weak to
	  the function symbol, not to the return type

2007-05-17 07:03 +0000 [r665565]  wstephens

	* branches/KDE/3.5/kdelibs/kdecore/kstringhandler.cpp: Fix broken
	  encoding of '\!' here too.

2007-05-19 14:04 +0000 [r666333]  bram

	* branches/KDE/3.5/kdelibs/kate/data/pascal.xml: Highlight 'read'
	  and 'write', used when declaring properties.

2007-05-22 09:00 +0000 [r667229]  hasso

	* branches/KDE/3.5/kdelibs/kjs/configure.in.in: Fix the tests. With
	  previous tests actual calls were optimized away with some
	  compilers causing errors in platforms where there is no isnan()
	  and/or isinf(). Thanks to Joerg Sonnenberger for pointers.

2007-05-25 12:40 +0000 [r668198]  lunakl

	* branches/KDE/3.5/kdelibs/kinit/Makefile.am,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdelibs/kinit/start_kdeinit.c,
	  branches/KDE/3.5/kdelibs/kinit/start_kdeinit_wrapper.c (added):
	  Fix the problem that the setuid kdeinit wrapper causes unsetting
	  some variables like LD_LIBRARY_PATH, I knew there would be
	  somebody who'd know what to do with it - add yet another wrapper
	  that saves the environment and use it again from withing the
	  setuid wrapper.

2007-05-26 22:31 +0000 [r668607]  dhaumann

	* branches/KDE/3.5/kdelibs/kate/part/kateviewinternal.cpp,
	  branches/KDE/3.5/kdelibs/kate/part/kateview.cpp: fix bug: Code
	  folding screws up highlighting Converting from/to virtual cursors
	  was wrong. Patch by: Ruud Koolen - Thanks a lot! BUG: 139578

2007-05-27 10:16 +0000 [r668699]  carewolf

	* branches/KDE/3.5/kdelibs/khtml/html/html_elementimpl.cpp:
	  Backport of GMail/Firefox fix

2007-05-27 19:59 +0000 [r668824]  dhaumann

	* branches/KDE/3.5/kdelibs/kate/part/katerenderer.cpp,
	  branches/KDE/3.5/kdelibs/kate/part/kateviewinternal.cpp: fix:
	  tabs in wrapped line broke cursor navigation Is there a
	  better/faster way? - todo: forward port Thanks to Ruud Koolen for
	  the patch! CCMAIL: kwrite-devel@kde.org BUG: 94693

2007-05-28 10:31 +0000 [r669014-669006]  mueller

	* branches/KDE/3.5/kdelibs/kio/kio/knfsshare.cpp: its a pointer,
	  don't use > on it

	* branches/KDE/3.5/kdelibs/libkmid/midimapper.h,
	  branches/KDE/3.5/kdelibs/libkmid/alsaout.h,
	  branches/KDE/3.5/kdelibs/libkmid/midiout.h,
	  branches/KDE/3.5/kdelibs/libkmid/deviceman.h,
	  branches/KDE/3.5/kdelibs/libkmid/player.h,
	  branches/KDE/3.5/kdelibs/libkmid/midistat.h,
	  branches/KDE/3.5/kdelibs/libkmid/voiceman.h,
	  branches/KDE/3.5/kdelibs/libkmid/notearray.h,
	  branches/KDE/3.5/kdelibs/libkmid/track.h: pedantic--

	* branches/KDE/3.5/kdelibs/kdeui/kdockwidget.h,
	  branches/KDE/3.5/kdelibs/kdeui/kcmodule.h,
	  branches/KDE/3.5/kdelibs/kdeui/kpanelextension.h,
	  branches/KDE/3.5/kdelibs/kdeui/kpanelapplet.h,
	  branches/KDE/3.5/kdelibs/kdeui/qxembed.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/kpixmapregionselectorwidget.h,
	  branches/KDE/3.5/kdelibs/kdeui/kmainwindow.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/kstatusbar.h,
	  branches/KDE/3.5/kdelibs/kdeui/kcombobox.h,
	  branches/KDE/3.5/kdelibs/kdeui/kdatetbl.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/kdatepicker.h,
	  branches/KDE/3.5/kdelibs/kdeui/kcolordrag.h,
	  branches/KDE/3.5/kdelibs/kdeui/kdockwidget_private.h,
	  branches/KDE/3.5/kdelibs/kdeui/kactionselector.h,
	  branches/KDE/3.5/kdelibs/kdeui/kbuttonbox.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/kfontcombo.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/kwindowlistmenu.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/kpixmapregionselectordialog.h:
	  pedantic--

	* branches/KDE/3.5/kdelibs/dcop/dcopserver.cpp: simplification

	* branches/KDE/3.5/kdelibs/dcop/dcopclient.cpp: dcop connections
	  must not be shared via exec.

	* branches/KDE/3.5/kdelibs/interfaces/ktexteditor/selectioninterfaceext.cpp,
	  branches/KDE/3.5/kdelibs/interfaces/kscript/scriptmanager.cpp:
	  pedantic--

	* branches/KDE/3.5/kdelibs/kdesu/process.h,
	  branches/KDE/3.5/kdelibs/kdesu/kcookie.cpp: its a pointer, don't
	  use ">" on it

	* branches/KDE/3.5/kdelibs/kutils/kcmoduleinfo.cpp,
	  branches/KDE/3.5/kdelibs/kutils/kcmoduleinfo.h,
	  branches/KDE/3.5/kdelibs/kutils/kplugininfo.cpp,
	  branches/KDE/3.5/kdelibs/kutils/kcmodulecontainer.cpp: use
	  KDesktopFile for parsing desktop files

	* branches/KDE/3.5/kdelibs/kabc/ldapurl.h,
	  branches/KDE/3.5/kdelibs/kabc/ldif.h: compiler warnings--

2007-05-28 10:41 +0000 [r669017]  mueller

	* branches/KDE/3.5/kdelibs/kioslave/ftp/ftp.cc: make sure m_size
	  isn't wrong when parsing has failed

2007-05-28 10:57 +0000 [r669021]  dhaumann

	* branches/KDE/3.5/kdelibs/kate/plugins/isearch/ISearchPlugin.cpp:
	  fix the broken search history of the isearch plugin. Thanks for
	  the patch. CCMAIL: hg@technosis.de

2007-05-29 19:11 +0000 [r669594]  aacid

	* branches/KDE/3.5/kdelibs/kdecore/kcmdlineargs.cpp: backport SVN
	  commit 669593 by dfaure: Reworked KCmdLineArgs::makeURL to make
	  "kpdf a:b" work when a:b is an existing file in the current
	  directory BUGS: 111760, 146105

2007-05-30 08:40 +0000 [r669739]  hasso

	* branches/KDE/3.5/kdelibs/kdecore/kgrantpty.c: DragonFly needs the
	  same hack as FreeBSD.

2007-05-30 18:36 +0000 [r669899]  lunakl

	* branches/KDE/3.5/kdebase/kicker/applets/minipager/pagerapplet.h,
	  branches/KDE/3.5/kdelibs/kdecore/netwm.cpp,
	  branches/KDE/3.5/kdebase/kwin/workspace.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/netwm_def.h,
	  branches/KDE/3.5/kdelibs/kdecore/netwm_p.h,
	  branches/KDE/3.5/kdelibs/kdecore/netwm.h,
	  branches/KDE/3.5/kdebase/kwin/workspace.h,
	  branches/KDE/3.5/kdebase/kwin/events.cpp,
	  branches/KDE/3.5/kdebase/kicker/applets/minipager/pagerapplet.cpp:
	  Backport _NET_DESKTOP_LAYOUT support from trunk. This will allow
	  mixing kde3/kde4 KWin and also fix #146040. BUG: 146040

2007-05-30 22:38 +0000 [r669977]  cartman

	* branches/KDE/3.5/kdelibs/kio/kio/defaultprogress.cpp: Do the
	  translation in one go, fixes Turkish translation, thanks to toma
	  and chusslove.

2007-06-01 04:20 +0000 [r670335]  tyrerj

	* branches/KDE/3.5/kdelibs/kdecore/kiconloader.cpp: BUG:53052

2007-06-01 13:32 +0000 [r670446]  mlaurent

	* branches/KDE/3.5/kdelibs/kdeprint/management/kxmlcommanddlg.cpp:
	  Fix bug: readonly and use html

2007-06-04 11:02 +0000 [r671262]  marten

	* branches/KDE/3.5/kdelibs/khtml/khtmlview.cpp: Fix confused cursor
	  shapes if mail and new-window links appear on the same page.
	  Backport of 671045 from trunk, without the option for additional
	  shapes (3.5 no more new features).

2007-06-04 14:05 +0000 [r671325]  mueller

	* branches/KDE/3.5/kdelibs/kdecore/kapplication.cpp: fix memory
	  leak

2007-06-05 21:43 +0000 [r671953]  thiago

	* branches/KDE/3.5/kdelibs/kinit/wrapper.c: Backport 671951: Make
	  kdeinit4_shutdown wait for kdeinit4's exit. Since kdeinit4
	  doesn't explicitly close the socket, it gets closed when the
	  program exits. So, this read_socket call returns when kdeinit4
	  has finished.

2007-06-07 09:14 +0000 [r672480]  mlaurent

	* branches/KDE/3.5/kdelibs/libkscreensaver/main.cpp: Fix mem leak

2007-06-10 11:00 +0000 [r673486]  toma

	* branches/KDE/3.5/kdelibs/kdoctools/customization/nl/user.entities:
	  Add Jordy entities.

2007-06-11 08:49 +0000 [r673872]  hasso

	* branches/KDE/3.5/kdelibs/libltdl/ltdl.m4: Unbreak KDE in
	  DragonFly BSD. I don't understand how I could miss this in
	  previous round.

2007-06-12 12:38 +0000 [r674421]  lunakl

	* branches/KDE/3.5/kdelibs/kdecore/kapplication.cpp: Reduce the
	  number of times _NET_STARTUP_ID is updated on windows. I don't
	  want to risk trying the real fix for KDE3.x.

2007-06-13 22:56 +0000 [r675314]  orlovich

	* branches/KDE/3.5/kdelibs/kio/kio/kmimemagic.cpp: Be a bit more
	  trusting of some tags when trying to autodetect the mimetype...
	  ... which not uncommonly happens due to sucky servers (Common
	  with ads -- there are some on digg.com, sports.yahoo.com...)
	  CCBUG:61483

2007-06-17 01:19 +0000 [r676516]  gamaral

	* branches/KDE/3.5/kdelibs/kate/part/katedocument.cpp,
	  branches/KDE/3.5/kdelibs/kate/part/katedocument.h: BUG: 143754
	  Fixed using pattern checking, it's been working great all day and
	  made it easy to modify.

2007-06-17 03:02 +0000 [r676539]  gamaral

	* branches/KDE/3.5/kdelibs/kate/part/katedocument.cpp: Unsigned to
	  signed comparison warning fix

2007-06-18 18:01 +0000 [r677226]  dfaure

	* branches/KDE/3.5/kdelibs/kstyles/plastik/plastik.h: Fix bug
	  spotted by Christian Spiel

2007-06-19 00:02 +0000 [r677351]  gamaral

	* branches/KDE/3.5/kdelibs/kate/part/katedocument.cpp: Added more
	  elavorate commentary on the inner workings of updateModified()

2007-06-19 00:25 +0000 [r677357]  gamaral

	* branches/KDE/3.5/kdelibs/kate/part/katedocument.cpp: Added new
	  patters found to updateModified()

2007-06-20 17:46 +0000 [r678100]  pley

	* branches/KDE/3.5/kdelibs/kio/kio/klimitediodevice.h: Fix possible
	  endless loop with broken archives.

2007-06-20 23:44 +0000 [r678255]  gamaral

	* branches/KDE/3.5/kdelibs/kate/part/katedocument.cpp: Added new
	  KateDocument::updateModified() code

2007-06-21 15:54 +0000 [r678521]  lunakl

	* branches/KDE/3.5/kdelibs/kdecore/netwm.cpp: Add missing detection
	  of changes for some properties. BUG: 146513

2007-06-25 10:00 +0000 [r680020]  grossard

	* branches/KDE/3.5/kdelibs/kdoctools/customization/fr/user.entities:
	  Added a translator

2007-06-29 08:36 +0000 [r681518]  ossi

	* branches/KDE/3.5/kdelibs/kdecore/kprocess.h: backport: document
	  limitations. for the sake of completeness ...

2007-06-30 13:33 +0000 [r681835]  orlovich

	* branches/KDE/3.5/kdelibs/khtml/css/cssstyleselector.cpp: Fix
	  handling of font-variant:inherit CCBUG:147226

2007-06-30 14:15 +0000 [r681861]  orlovich

	* branches/KDE/3.5/kdelibs/khtml/ecma/kjs_events.cpp: Properly
	  emulate getting of returnValue. Fixes typing in input boxes on
	  store locator on riteaid.com CCBUG:141230

2007-07-04 23:31 +0000 [r683564]  dfaure

	* branches/KDE/3.5/kdelibs/kio/tests/kmimetypetest.cpp,
	  branches/KDE/3.5/kdelibs/kio/kio/kservicetypefactory.cpp: Fix
	  diagnoseMimeType bug found in kde4 by aacid: it included trailing
	  spaces due to the padding done internally in ksycoca.

2007-07-11 20:46 +0000 [r686629]  nlecureuil

	* branches/KDE/3.5/kdelibs/mimetypes/application/Makefile.am:
	  Install existing x-zip-compressed.desktop file

2007-07-15 09:39 +0000 [r688176]  lukas

	* branches/KDE/3.5/kdelibs/khtml/khtml_ext.cpp: missing i18n

2007-07-15 15:41 +0000 [r688255]  mueller

	* branches/KDE/3.5/kdelibs/kio/kio/kmimetyperesolver.h: pedantic--

2007-07-16 11:45 +0000 [r688594]  binner

	* branches/KDE/3.5/kdelibs/kio/kfile/kfilesharedlg.cpp,
	  branches/KDE/3.5/kdelibs/kutils/kcmoduleproxy.cpp: ensure that
	  kcmshell matching the running KDE version is called

2007-07-17 09:55 +0000 [r688949]  rdale

	* branches/KDE/3.5/kdelibs/kate/data/rhtml.xml: * Rails edge now
	  uses .html.erb as an extension instead of .rhtml, so add that.
	  Fixes problem reported by Patrick Aljord. CCMAIL:
	  patcito@gmail.com

2007-07-18 22:50 +0000 [r689709]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/http.cc: - Fix crash
	  described by BUG# 130104. BUG:130104

2007-07-23 21:38 +0000 [r691544]  pino

	* branches/KDE/3.5/kdelibs/kdecore/kaccelmanager.cpp: Backport of
	  SVN commit 691531 by aacid: Make KAcceleratorManager automanage
	  popups See
	  http://lists.kde.org/?l=kde-core-devel&m=118434863104856&w=2 on
	  k-c-d

2007-07-24 09:29 +0000 [r691734]  rdale

	* branches/KDE/3.5/kdelibs/kate/data/ruby.xml: * Added suffixes of
	  '*.xml.erb' and '*.js.erb' as they replace '*.rxml' and '*.rjs'
	  respectively CCMAIL: patcito@gmail.com

2007-07-24 17:19 +0000 [r691942]  grossard

	* branches/KDE/3.5/kdelibs/kdoctools/customization/fr/user.entities:
	  added a french translator

2007-07-29 16:28 +0000 [r693955]  tyrerj

	* branches/KDE/3.5/kdelibs/kdecore/kiconloader.cpp: Appling the fix
	  for bug 53052 to KIconLoader::moviePath Cleaning up code by
	  adding 'goto's

2007-07-29 20:52 +0000 [r694030]  nixternal

	* branches/KDE/3.5/kdelibs/doc/common/kde-default.css: BUG:147814
	  Fix padding on right side in KHelpCenter to improve readability

2007-08-05 04:11 +0000 [r696502]  charles

	* branches/KDE/3.5/kdelibs/khtml/dom/dom2_events.cpp: the js
	  keyboard event (mozilla extension) property event returns 0 if
	  the key pressed cannot be expressed as an ascii code
	  (apparently). This behavior is the same as Firefox, /more/
	  similar to Opera (although opera appears to have an
	  implementation of "event" more buggy than konqueror as a result
	  of this patch). I figure firefox's implementation is canonical as
	  it's a gecko extension. Still, there's a remaining bug in that
	  you can't send "ctrl-something" because Konqueror filters it away
	  from the Part. I'm not going to fix that.

2007-08-10 18:22 +0000 [r698691]  mueller

	* branches/KDE/3.5/kdelibs/kinit/kinit.cpp,
	  branches/KDE/3.5/kdelibs/kinit/setproctitle.cpp: fix consolekit
	  support

2007-08-12 11:35 +0000 [r699230]  mueller

	* branches/KDE/3.5/kdelibs/libltdl/ltdl.c: be more robust against
	  NUL bytes occuring in .la files

2007-08-14 12:14 +0000 [r699954]  dfaure

	* branches/KDE/3.5/kdelibs/kdeui/kcharselect.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/kcolordialog.cpp: Use KLineEdit
	  instead of QLineEdit so that Ctrl+Insert works for copying out of
	  those fields (I copy all the time from kcolorchooser, so it's
	  frustrating when Ctrl+Insert doesn't work and I have to remember
	  to use Ctrl+C...)

2007-08-14 16:37 +0000 [r700053]  mueller

	* branches/KDE/3.5/kdelibs/kdecore/kurl.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/tests/kurltest.cpp: be more
	  robust against addressbar spoofing (CVE-2007-4225)

2007-08-15 11:58 +0000 [r700380]  klichota

	* branches/KDE/3.5/kdelibs/kdeui/kswitchlanguagedialog.h (added),
	  branches/KDE/3.5/kdelibs/kdeui/kstdaction_p.h,
	  branches/KDE/3.5/kdelibs/kdeui/Makefile.am,
	  branches/KDE/3.5/kdelibs/kdeui/khelpmenu.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/kstdaction.h,
	  branches/KDE/3.5/kdelibs/kdecore/klocale.h,
	  branches/KDE/3.5/kdelibs/kdeui/khelpmenu.h,
	  branches/KDE/3.5/kdelibs/kdeui/kswitchlanguagedialog.cpp (added),
	  branches/KDE/3.5/kdelibs/kdeui/ui_standards.rc,
	  branches/KDE/3.5/kdelibs/kdeui/kstdaction.cpp: Finally I have
	  found some time to recompile KDE and test the feature again. This
	  is re-commit of implementation of switching language for
	  individual applications. Previous commit (r647712) was reverted
	  as feature was not ported to KDE 4 then. Now it is. For language
	  switching option see menu Help->Switch application language.
	  Feature freeze exception was granted for this feature. See:
	  http://lists.kde.org/?l=kde-devel&m=118096509723997&w=2 Thanks to
	  release team for that :) Also thanks to David Faure for review
	  and providing valuable tips. CCBUG: 24348 FEATURE: 24348 GUI:

2007-08-16 10:19 +0000 [r700738]  mueller

	* branches/KDE/3.5/kdelibs/libltdl/ltdl.c: make sure we're not
	  testing uninitialized memory after a realloc

2007-08-16 12:57 +0000 [r700792]  mueller

	* branches/KDE/3.5/kdelibs/kdecore/kaudioplayer.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kstartupinfo.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kglobal.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kaccelmanager_private.h,
	  branches/KDE/3.5/kdelibs/kdecore/kaccelmanager.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kconfigbackend.h,
	  branches/KDE/3.5/kdelibs/kdecore/network/ksocketdevice.h,
	  branches/KDE/3.5/kdelibs/kdecore/kstringhandler.h: pedantic--

2007-08-20 09:16 +0000 [r702252]  bvirlet

	* branches/KDE/3.5/kdelibs/kabc/ldifconverter.cpp,
	  branches/KDE/3.5/kdelibs/kabc/vcardtool.cpp,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kaddressbook/ldapsearchdialog.cpp,
	  branches/KDE/3.5/kdepim/kaddressbook/addresseeeditorwidget.cpp,
	  branches/KDE/3.5/kdelibs/kabc/scripts/entrylist: Follow RFC
	  concerning ORG property. The patch will still read the old
	  Department values if they exist but store them correctly. BUG:
	  115129

2007-08-20 12:13 +0000 [r702333]  bvirlet

	* branches/KDE/3.5/kdelibs/kabc/ldifconverter.cpp: Fix (thank you
	  Christian). CCBUG: 115219

2007-08-21 13:56 +0000 [r702908]  hasso

	* branches/KDE/3.5/kdelibs/kspell2/plugins/ispell/ispell_checker.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/ksconfig.cpp: Make spellchecking
	  (both ispell and aspell) work on systems using pkgsrc like NetBSD
	  and DragonFly.

2007-08-22 08:24 +0000 [r703221]  dfaure

	* branches/KDE/3.5/kdelibs/kdeui/tests/kspelltest.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/kspelldlg.cpp: Fix memory leak.
	  It's created with d->spellConfig = new KSpellConfig( 0, 0 ,0,
	  false ) so it must be deleted manually.

2007-08-24 14:39 +0000 [r704289]  helio

	* branches/KDE/3.5/kdelibs/kdeprint/cups/cupsdconf2/cups-util.c,
	  branches/KDE/3.5/kdelibs/kdeprint/cups/ipprequest.cpp: - Make it
	  compile against cups >= 1.2, including brand new 1,3

2007-08-24 21:16 +0000 [r704432-704431]  mueller

	* branches/KDE/3.5/kdelibs/dcop/dcopserver.cpp: improve debug
	  output

	* branches/KDE/3.5/kdelibs/kdoctools/kio_help.cpp,
	  branches/KDE/3.5/kdelibs/kdoctools/kio_help.h: - avoid expanding
	  substituted %'s

2007-08-24 21:23 +0000 [r704433]  mueller

	* branches/KDE/3.5/kdelibs/kio/kio/ksambashare.cpp: its a pointer

2007-08-26 02:05 +0000 [r704720]  charles

	* branches/KDE/3.5/kdelibs/khtml/rendering/bidi.cpp: don't
	  sometimes take text far past the end of the block's margin on
	  justified text. I committed a patch which does this exact same
	  thing in 2005 (484974), but for whatever reason, it seemed to be
	  reproducable again, so here's the "rest" of it.

2007-08-26 17:19 +0000 [r704939]  helio

	* branches/KDE/3.5/kdelibs/kdeprint/cups/ipprequest.cpp: - Make it
	  compile back with 1.2.x

2007-08-27 14:04 +0000 [r705224]  woebbe

	* branches/KDE/3.5/kdelibs/kdeprint/cups/ipprequest.cpp: compile

2007-08-28 11:45 +0000 [r705705]  krake

	* branches/KDE/3.5/kdelibs/kabc/addressbook.cpp: - Backporting fix
	  to AddressBook::resourceSavingFinished() as in Rev 705704 in
	  trunk

2007-08-29 17:22 +0000 [r706197]  helio

	* branches/KDE/3.5/kdelibs/kdeprint/cups/ipprequest.cpp: - tssc,
	  tssc, trying to write on wrong place is not a good idea. Cups 1.3
	  is now fully functional and i kick myself for this mess...

2007-08-30 19:27 +0000 [r706570-706567]  boiko

	* branches/KDE/3.5/kdelibs/kdecore/svgicons/ksvgiconpainter.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/svgicons/ksvgiconengine.cpp: Fix
	  loading images and add support for loading images from external
	  refs Fix also the positioning of the image (which should have
	  been proper translated to the global coordinates before drawn)

	* branches/KDE/3.5/kdelibs/kdeui/kactionclasses.cpp: Use the
	  activated(int) signal instead of activated(QString) preventing
	  wrong selection of items that have the same name (this problem
	  can be seen in kaffeine when selecting the subtitle and audio
	  tracks, some DVDs have tracks using the same name).

2007-09-01 09:04 +0000 [r707211]  hasso

	* branches/KDE/3.5/kdelibs/kdeprint/management/smbview.cpp:
	  Backport 707209. Fix browsing SMB printers. -i is not POSIX and
	  doesn't exist in most of xargs implementations, -I should be used
	  instead. Patch by Kris Moore <kris@pcbsd.com>.

2007-09-01 17:19 +0000 [r707375]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/kcookiejar/tests/cookie.test,
	  branches/KDE/3.5/kdelibs/kioslave/http/kcookiejar/kcookieserver.cpp,
	  branches/KDE/3.5/kdelibs/kioslave/http/kcookiejar/kcookiejar.cpp:
	  - Fix BR# 134421 and as a result fix a cross-site cookie
	  injection vulnerability. - Simplyfy the code that saves the
	  contents of the cookiejar in KCookieServer.

2007-09-01 17:30 +0000 [r707381]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/http.cc,
	  branches/KDE/3.5/kdelibs/kioslave/http/http.h: BACKPORT: fix for
	  correctly propagating HTTP response headers > 4K

2007-09-01 20:05 +0000 [r707413]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/http.cc: - Backport fix
	  for BR# 147812. - Propagate HTTP repsonse header before
	  processing or it might never get propagated up.

2007-09-01 21:04 +0000 [r707428]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/kcookiejar/tests/cookie.test,
	  branches/KDE/3.5/kdelibs/kioslave/http/kcookiejar/kcookiejar.cpp:
	  - Backport fix and test case for BR# 134753.

2007-09-02 04:43 +0000 [r707523]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/http.cc: - Do not check
	  for m_bEOF when reading chunked data. m_bEOF is set true when
	  read returns 0, which in chunked data transfer indicates end of
	  chunked data transfer. BUG: 124593

2007-09-03 00:41 +0000 [r707799]  klichota

	* branches/KDE/3.5/kdelibs/kdeui/khelpmenu.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/kstdaction.h: Fix for "Switch
	  application language" menu entry not appearing in almost all
	  apps, apart from Konsole. This fixes it for most apps, the only
	  ones which does not seem fixed are kwrite and kate. Thanks to
	  Stephan Johach for spotting the problem.

2007-09-03 07:52 +0000 [r707873]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/kcookiejar/kcookiejar.cpp:
	  Backport the fix for BR# 135175

2007-09-05 22:08 +0000 [r708880]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/kcookiejar/kcookiejar.cpp:
	  - Send back automatically accepted session cookies. It makes no
	  sense to automatically accept session cookies if we are not going
	  to automatically send them back regardless of the current global
	  or per-site domain policy. Fixes a long standing bug report. BUG:
	  86208

2007-09-07 17:48 +0000 [r709515]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/http.cc: - Do not forward
	  http response twice by mistake! This fix the crash in kopete when
	  logging into MSN as reported by folks @ Mandriva.

2007-09-07 17:53 +0000 [r709516]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/kcookiejar/kcookiejar.cpp:
	  Fix for BR# 145244. Persistent cookies are treated like session
	  cookies. BUG: 145244

2007-09-08 10:51 +0000 [r709835]  savernik

	* branches/KDE/3.5/kdelibs/kdeprint/cups/cupsdconf2/cups-util.c:
	  Restore CUPS support < 1.2.0

2007-09-10 18:09 +0000 [r710724]  carewolf

	* branches/KDE/3.5/kdelibs/khtml/ChangeLog,
	  branches/KDE/3.5/kdelibs/khtml/css/cssstyleselector.h,
	  branches/KDE/3.5/kdelibs/khtml/css/cssstyleselector.cpp: Backport
	  of r710723 fix for Trolltech invalid XHTML

2007-09-10 19:09 +0000 [r710740]  helio

	* branches/KDE/3.5/kdelibs/kdeprint/filters/pdf2ps.desktop (added),
	  branches/KDE/3.5/kdelibs/kdeprint/filters/Makefile.am,
	  branches/KDE/3.5/kdelibs/kdeprint/filters/pdf2ps.xml (added): -
	  Trying convert pdf to ps files fail since there's no proper
	  filter to convert. Ghostscript provides same pdf2ps utility in
	  their package

2007-09-10 21:32 +0000 [r710782]  carewolf

	* branches/KDE/3.5/kdelibs/khtml/css/cssstyleselector.cpp: Revert
	  stuff that shouldn't have been part of last commit

2007-09-11 09:42 +0000 [r711061]  tpatzig

	* branches/KDE/3.5/kdelibs/kded/kded.cpp: -fix destruktor to delete
	  all modules

2007-09-12 00:00 +0000 [r711304]  mueller

	* branches/KDE/3.5/kdelibs/kdecore/kurl.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/tests/kurltest.cpp: shorten
	  username in prettyUrl() output if no password is set

2007-09-12 08:48 +0000 [r711539]  mueller

	* branches/KDE/3.5/kdelibs/kdecore/kconfig_compiler/tests/test9main.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kconfig_compiler/tests/test1main.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kconfig_compiler/tests/test2main.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kconfig_compiler/tests/test3main.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kconfig_compiler/tests/test_dpointer_main.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kconfig_compiler/tests/test4main.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kconfig_compiler/tests/test5main.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kconfig_compiler/tests/test6main.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kconfig_compiler/tests/test7main.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kconfig_compiler/tests/test8main.cpp:
	  fix build

2007-09-12 14:38 +0000 [r711659]  dhaumann

	* branches/KDE/3.5/kdelibs/kate/data/cpp.xml: BUG: 149801

2007-09-13 21:33 +0000 [r712197]  aacid

	* branches/KDE/3.5/kdelibs/khtml/rendering/render_block.cpp: Patch
	  by Germain Garand to fix the bug i reported yesterday, you rock
	  ;-) In his words "The git of it is to skip positioned/floating
	  objects while scanning for inlines." "Ported" from WebCore BUGS:
	  149809

2007-09-14 11:59 +0000 [r712434]  mueller

	* branches/KDE/3.5/kdelibs/libltdl/ltdl.c: fix NUL placement

2007-09-14 20:28 +0000 [r712586]  orlovich

	* branches/KDE/3.5/kdelibs/khtml/khtmlview.cpp: Rever the cosmetic
	  bugfix that caused the major regression in editting of WordPress
	  articles. The textarea-level fix is too risky for 3.5.x, and will
	  only go into 4.x CCBUG:141457

2007-09-18 17:05 +0000 [r714066]  lunakl

	* branches/KDE/3.5/kdelibs/dcop/dcopclient.cpp,
	  branches/KDE/3.5/kdelibs/kio/kio/slavebase.cpp,
	  branches/KDE/3.5/kdelibs/dcop/dcopclient.h: Process incomming
	  dcop calls in kioslaves, otherwise they'll block broadcasts done
	  with DCOPClient::call() (bnc:301648).

2007-09-21 15:08 +0000 [r715236]  mueller

	* branches/KDE/3.5/kdelibs/kdecore/kwin.cpp:

2007-09-21 17:27 +0000 [r715274]  perreaul

	* branches/KDE/3.5/kdelibs/kdecore/kextsock.cpp: A hostname may
	  resolve to multiple addresses. When connection to one timeouts,
	  we should not immediately give up. Instead, we try the other
	  addresses.

2007-09-21 19:07 +0000 [r715296]  helio

	* branches/KDE/3.5/kdelibs/kdeprint/filters/pdf2ps.xml: - Proper
	  fix to pdf2ps conversion. Thanks to Ademar Reis for find the
	  issue.

2007-09-21 20:18 +0000 [r715315]  dfaure

	* branches/KDE/3.5/kdelibs/kio/kio/karchive.cpp: Fix extraction of
	  symlinks from tar/zip files when using copyTo() - at least at the
	  KArchive level. Unit tests in trunk. CCBUG: 149903

2007-09-23 14:29 +0000 [r715937]  pino

	* branches/KDE/3.5/kdelibs/kate/data/cpp.xml: add Q_PRIVATE_SLOT,
	  Q_DECLARE_METATYPE, and Q_NOREPLY

2007-09-25 20:52 +0000 [r717004]  dfaure

	* branches/KDE/3.5/kdelibs/kio/kio/kfileitem.cpp: Konqueror
	  performance patch 1 (especially on a smbmount'ed directory):
	  avoid a stat() call in KFileItem::isReadable() when possible

2007-09-26 08:02 +0000 [r717174]  grossard

	* branches/KDE/3.5/kdelibs/kdoctools/customization/fr/user.entities:
	  copied from trunk branch (added &nbsp;)

2007-09-26 17:08 +0000 [r717341]  carewolf

	* branches/KDE/3.5/kdelibs/kioslave/http/http.cc,
	  branches/KDE/3.5/kdelibs/kioslave/http/http.h: Revert r707381
	  which broke http-headers, and add a bit more buffer to handle
	  long http-headers like X-JSON which r707381 tried to fix.

2007-09-26 21:25 +0000 [r717418]  porten

	* branches/KDE/3.5/kdelibs/kioslave/ftp/ftp.cc: Albeit the RFC
	  mandates a case-insensitive handling of command names it contains
	  only upper case versions. And some stupid servers (like our
	  Brother printer) can only deal with this style. Verified that the
	  Linux ftp command client sends e.g. "LIST" on "ls", too.

2007-09-26 21:40 +0000 [r717437]  grossard

	* branches/KDE/3.5/kdelibs/kdoctools/customization/fr/entities/underFDL.docbook,
	  branches/KDE/3.5/kdelibs/kdoctools/customization/fr/entities/fdl-notice.docbook:
	  minor change to the link to GNU Free Documentation License

2007-09-29 20:20 +0000 [r718830]  adawit

	* branches/KDE/3.5/kdelibs/khtml/ecma/xmlhttprequest.cpp: * Default
	  to "UTF-8" per section 2 of the draft W3C "The XMLHttpRequest
	  Object" specification. Fixes BR# 130234 BUG:130234

2007-09-30 12:01 +0000 [r719137]  annma

	* branches/KDE/3.5/kdelibs/kdoctools/customization/fr/user.entities:
	  fix wrong entity

2007-09-30 12:12 +0000 [r719139]  annma

	* branches/KDE/3.5/kdelibs/kdoctools/customization/entities/general.entities:
	  kmenuedit -> KMenuEdit to be consistent with the rest of KDE

2007-10-01 21:55 +0000 [r719788]  grossard

	* branches/KDE/3.5/kdelibs/kdoctools/customization/fr/user.entities:
	  added a translator (Jean-Jacques Finazzi)

2007-10-02 04:21 +0000 [r719916]  orlovich

	* branches/KDE/3.5/kdelibs/kjs/function.cpp: Fix a bad regression
	  caused by a bug in the stricter utf-8 validation code that caused
	  us to replace latin1 glyphs (e.g. accented chars) with the
	  unicode replacement character. BUG:150381

2007-10-08 11:05 +0000 [r722967]  coolo

	* branches/KDE/3.5/kdelibs/kdelibs.lsm: updating lsm

2007-10-08 11:10 +0000 [r722982-722981]  coolo

	* branches/KDE/3.5/kdelibs/kdecore/kdeversion.h: update for tag

	* branches/KDE/3.5/kdelibs/README: update for tag

2007-10-08 11:23 +0000 [r722988]  coolo

	* branches/KDE/3.5/kdelibs/kdeui/kdepackages.h: update

