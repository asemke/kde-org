------------------------------------------------------------------------
r1227671 | shaforo | 2011-04-12 08:28:03 +1200 (Tue, 12 Apr 2011) | 5 lines

BUG:265086

backport wordwrapping setting for those who consider this urgent


------------------------------------------------------------------------
r1227782 | cgiboudeaux | 2011-04-12 22:21:54 +1200 (Tue, 12 Apr 2011) | 4 lines

Backport r1227781 from trunk to 4.6:
Also find hunspell 1.3


------------------------------------------------------------------------
r1227925 | scripty | 2011-04-15 02:20:19 +1200 (Fri, 15 Apr 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1228260 | dhaumann | 2011-04-17 01:49:14 +1200 (Sun, 17 Apr 2011) | 4 lines

backport fix: colorize buttons works again in flat mode

CCBUG: 268425

------------------------------------------------------------------------
r1228262 | dhaumann | 2011-04-17 02:19:57 +1200 (Sun, 17 Apr 2011) | 1 line

fix xml gui factory warnings on application exit or plugin unload
------------------------------------------------------------------------
r1228263 | dhaumann | 2011-04-17 02:32:01 +1200 (Sun, 17 Apr 2011) | 1 line

backport: remember highlighted tab buttons
------------------------------------------------------------------------
r1229551 | scripty | 2011-04-28 04:45:01 +1200 (Thu, 28 Apr 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
