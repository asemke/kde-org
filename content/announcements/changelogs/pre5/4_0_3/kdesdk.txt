2008-03-02 13:27 +0000 [r781270]  dhaumann

	* branches/KDE/4.0/kdesdk/kate/plugins/tabbarextension/ktinytabbar.cpp:
	  Change default tab height to 22px. CCBUG: 158673

2008-03-10 23:28 +0000 [r784343]  lunakl

	* branches/KDE/4.0/kdeedu/kwordquiz/src/kwordquiz.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstarsactions.cpp,
	  branches/KDE/4.0/kdeedu/kig/kig/kig_view.cpp,
	  branches/KDE/4.0/kdesdk/kate/app/katemainwindow.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/QtMainWindow.cpp,
	  branches/KDE/4.0/kdegames/kgoldrunner/src/kgoldrunner.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/chatview.cpp,
	  branches/KDE/4.0/kdegraphics/kolourpaint/mainWindow/kpMainWindow_Settings.cpp,
	  branches/KDE/4.0/kdegraphics/okular/shell/shell.cpp,
	  branches/KDE/4.0/kdegames/ktuberling/toplevel.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/marble_part.cpp,
	  branches/KDE/4.0/kdeedu/kmplot/kmplot/kmplot.cpp: Backport
	  r784333 (remove usage of QWidget::showFullScreen() etc.), use new
	  KToggleFullScreenAction::setFullScreen() helper.

2008-03-14 15:35 +0000 [r785650-785649]  woebbe

	* branches/KDE/4.0/kdesdk/cervisia/logdialog.cpp,
	  branches/KDE/4.0/kdesdk/cervisia/progressdialog.cpp: --qt3support

	* branches/KDE/4.0/kdesdk/cervisia/version.h: bump version number

2008-03-25 17:13 +0000 [r789919]  jriddell

	* branches/KDE/4.0/kdesdk/umbrello/VERSION: update version

2008-03-27 19:30 +0000 [r790886]  alund

	* branches/KDE/4.0/kdesdk/kate/plugins/filebrowser/katefileselector.cpp:
	  backport fix for #158704

2008-03-27 19:35 +0000 [r790888]  alund

	* branches/KDE/4.0/kdesdk/kate/app/kateapp.cpp: Backport fix for
	  #159767

