---
aliases:
- ../../plasma-5.14.4
changelog: 5.14.3-5.14.4
date: 2018-11-27
layout: plasma
figure:
  src: /announcements/plasma/5/5.14.0/plasma-5.14.png
asBugfix: true
---

- Fix global progress display for updates. <a href="https://commits.kde.org/discover/119c3ffcba622cbc136ec0adcea8b7518379aed2">Commit.</a> Fixes bug <a href="https://bugs.kde.org/400891">#400891</a>
- [weather] Fix broken observation display for temperature of 0 &#176;. <a href="https://commits.kde.org/kdeplasma-addons/0d379c5957e2b69e34839535d1620651c1988e54">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D16977">D16977</a>
- [Folder View] improve label contrast against challenging backgrounds. <a href="https://commits.kde.org/plasma-desktop/10278e79f11677bd59f7d554eb8e18e580686082">Commit.</a> Fixes bug <a href="https://bugs.kde.org/361228">#361228</a>. Phabricator Code review <a href="https://phabricator.kde.org/D16968">D16968</a>
