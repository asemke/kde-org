---
aliases:
- ../../plasma-5.19.1
changelog: 5.19.0-5.19.1
date: 2020-06-16
layout: plasma
video: true
asBugfix: true
---

- Dr Konqi: Map neon in platform guessing. <a href="https://commits.kde.org/drkonqi/4f0544a2e92a9c01a5ba58a01e9c268318dab31f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/422556">#422556</a>
- Battery applet not showing up in tray. <a href="https://commits.kde.org/plasma-workspace/1c3dc48629d1b27df93244147f0a52a65dd8c0c9">Commit.</a>
- Fix confirmLogout setting for SessionManagement. <a href="https://commits.kde.org/plasma-workspace/d49e0a406857287658f205ed8b0aaf8bf31dbb80">Commit.</a>
