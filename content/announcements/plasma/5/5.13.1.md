---
aliases:
- ../../plasma-5.13.1
changelog: 5.13.0-5.13.1
date: 2018-06-19
layout: plasma
youtube: C2kR1_n_d-g
figure:
  src: /announcements/plasma/5/5.13.0/plasma-5.13.png
  class: text-center mt-4
asBugfix: true
---

- Discover: Fix build with newer flatpak. <a href="https://commits.kde.org/discover/e95083f4a3a3cc64be9c98913a42759bce2716ee">Commit.</a>

- Fix tooltip woes. <a href="https://commits.kde.org/plasma-desktop/24803dc9dc4000e06edb60a3a8afe5925f0d72d7">Commit.</a> Fixes bug <a href="https://bugs.kde.org/382571">#382571</a>. Fixes bug <a href="https://bugs.kde.org/385947">#385947</a>. Fixes bug <a href="https://bugs.kde.org/389469">#389469</a>. Fixes bug <a href="https://bugs.kde.org/388749">#388749</a>. Phabricator Code review <a href="https://phabricator.kde.org/D13602">D13602</a>
- Fonts KCM: Fix text readability regression. <a href="https://commits.kde.org/plasma-desktop/540edfdd2a88190a1f3665a952a1d2d57c72ab3f">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D13390">D13390</a>
