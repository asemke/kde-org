---
aliases:
- ../../plasma-5.17.2
changelog: 5.17.1-5.17.2
date: 2019-10-29
layout: plasma
peertube: 5a315252-2790-42b4-8177-94680a1c78fc
figure:
  src: /announcements/plasma/5/5.17.0/plasma-5.17.png
asBugfix: true
---

- [wallpapers/image] Randomise new batches of images in the slideshow. <a href="https://commits.kde.org/plasma-workspace/9dca7d6cd44cbb16c6d7fb1aca5588760544b1d6">Commit.</a> Fixes bug <a href="https://bugs.kde.org/413463">#413463</a>. Phabricator Code review <a href="https://phabricator.kde.org/D24986">D24986</a>
- [System Settings sidebar] Add a hover effect to intro page icons. <a href="https://commits.kde.org/systemsettings/a306b76cb8531905b463e33e52b1176c0073d4f1">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D24901">D24901</a>
- Appstream: support more formats of appstream urls. <a href="https://commits.kde.org/discover/4f2aa69241717a12e09bd49aba9ff78bd202960a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/408419">#408419</a>
