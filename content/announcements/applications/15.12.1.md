---
aliases:
- ../announce-applications-15.12.1
changelog: true
date: 2016-01-12
description: KDE Ships KDE Applications 15.12.1
layout: application
title: KDE Ships KDE Applications 15.12.1
version: 15.12.1
---

{{% i18n_var "January 12, 2016. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 15.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../15.12.0" %}}

More than 30 recorded bugfixes include improvements to kdelibs, kdepim, kdenlive, marble, konsole, spectacle, akonadi, ark and umbrello, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.16"%}}
