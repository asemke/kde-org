---
aliases:
- ../announce-2.1-beta1
custom_about: true
custom_contact: true
date: '2000-12-16'
description: KDE 2.1 will be the second major release of the KDE 2 series, the next
  generation of the award-winning K Desktop Environment.
title: KDE 2.1-beta1 Release Announcement
---

FOR IMMEDIATE RELEASE

<h3 ALIGN="center">New Beta KDE Release Available for Linux Desktop</h3>

New Beta Version of Leading Linux Desktop Offers New Theme
Manager, Image Viewer and IDE and Many Enhancements

December 16, 2000 (The INTERNET). The <a href="/">KDE
Team</a> today announced the release of KDE 2.1-beta1, a powerful, modular,
Internet-enabled desktop. KDE 2.1 constitutes the second major release of
the KDE 2 series, which is the next generation of the
<a href="/community/awards">award-winning</a> KDE 1
series. KDE is the work product of hundreds of dedicated developers
originating from over 30 countries.

KDE 2.1-beta1 offers a number of additions, enhancements and fixes over
KDE 2.0.1, the last stable KDE release which shipped on December 5, 2000.
The major additions are:

<UL>
<li>A new and much-anticipated theme manager has been added, and many icons
have been improved.  In addition, semi-transparency (alpha-blending) has
been implemented on small images and icons.</li>
<li><a href="http://www.mosfet.org/pixie/">Pixie</a>, an image viewer/editor, has been added to the Graphics package.</li>
<li><a href="http://www.kdevelop.org/">KDevelop</a>, a C/C++ integrated
development environment, has been added to the core KDE distribution.  The
version being shipped, 1.4beta, is the first version of KDevelop to
make use of the KDE 2 libraries and integrate completely with the KDE 2
desktop.</li>
<li><a href="http://konqueror.kde.org/">Konqueror</a>, the KDE 2 file
manager, can now <a href="http://www.mieterra.com/konqueror/malte2.png">be
configured</a> to provide thumbnail previews for
<a href="http://www.mieterra.com/konqueror/preview.png">text and HTML files</a>.
In addition, the standards-compliant Konqueror now stores bookmarks
using the standard <a href="http://grail.sourceforge.net/info/xbel.html">XBEL
bookmark format</a>; a new bookmark editor complements the new standard.
Finally, auto-proxy configuration has been implemented.</li>
<li><a href="http://developer.kde.org/kde2arch/khtml/index.html">KHTML</a>,
the HTML widget, now has a special 'transitional mode' which greatly improves
its handling of malformed HTML pages.  In additon, KHTML now has greatly
improved Java support and has added support for Java security (JDK 1.2 or
compatible is now required).</li>
<li>The panel (Kicker) has enjoyed significant improvements. 
An external taskbar has been included (familiar to
KDE 1 users), support for sub-panels has been added (which can be separately
sized and positioned), an improved external pager (Kasbar) has been added,
and support for applets has been improved (including support for
<a href="http://windowmaker.org/">WindowMaker</a> dock applets).</li>
<li><a href="http://www.arts-project.org/">ARts</a>, the KDE 2 multimedia
architecture, now offers a control module to configure sampling rate and
output devices, increased performance, improved user interfaces and a
number of additional effects and filters.</li>
<li>For developers, a number of classes have been added to the core
libraries, including a class for undo/redo support (KCommand) and
a class for editing list boxes (KEditListBox).</li>
</UL>

KDE 2.1-beta1 includes the core KDE libraries, the core desktop environment,
and KDevelop, as well as the over 100 applications from the other
standard base KDE packages: Administration, Games, Graphics, Multimedia,
Network, Personal Information Management (PIM), Toys and Utilities.
<a href="http://koffice.kde.org/">KOffice</a> is not included in this release.

All of KDE 2.1-beta1 is available for free under an Open Source license.
Likewise,
<a href="http://www.trolltech.com/">Trolltech's</a> Qt 2.2.x, the GUI
toolkit on which KDE is based,
is also available for free under two Open Source licenses: the
<a href="http://www.trolltech.com/products/download/freelicense/license.html">Q
Public License</a> and the <a href="http://www.gnu.org/copyleft/gpl.html">GNU
General Public License</a>.

A more complete
<a href="/announcements/changelogs/changelog2_0to2_1">list of
major changes</a>, a <a href="/info/2.1">FAQ about
the release</a> and the
<a href="http://developer.kde.org/development-versions/kde-2.1-release-plan.html">KDE
2.1 release plan</a> are available at the KDE
<a href="/">website</a>. More information about KDE 2
is available in a
<a href="http://devel-home.kde.org/~granroth/LWE2000/index.html">slideshow
presentation</a> and on
<a href="/">KDE's web site</a>, including a number of
<a href="../screenshots/kde2shots">screenshots</a>,
<a href="http://developer.kde.org/documentation/kde2arch.html">developer
information</a> and a developer's
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html?rev=2.5">KDE 1 - KDE 2 porting guide</a>.

#### Downloading and Compiling KDE

The source packages for KDE 2.1-beta1 are available for free download at
<a href="http://ftp.kde.org/unstable/distribution/2.1beta1/tar/generic/src/">http://ftp.kde.org/unstable/distribution/2.1beta1/tar/generic/src/</a> or in the
equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>. KDE 2.1-beta1 requires
qt-2.2.1, which is available from the above locations under the name
<a href="http://ftp.kde.org/stable/2.0/distribution/tar/generic/src/qt-x11-2.2.1.tar.gz">qt-x11-2.2.1.tar.gz</a>,
although
<a href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.2.3.tar.gz">qt-2.2.3</a>
is recommended. KDE 2.1-beta1 will not work with versions of Qt older
than 2.2.1.

For further instructions on compiling and installing KDE, please consult
the <a href="http://developer.kde.org/build/index.html">installation
instructions</a> and, if you encounter problems, the
<a href="http://developer.kde.org/build/index.html">compilation FAQ</a>.

#### Installing Binary Packages

Some distributors choose to provide binary packages of KDE for certain
versions of their distribution. Some of these binary packages for KDE 2.1-beta1
will be available for free download under
<a href="http://ftp.kde.org/unstable/distribution/2.1beta1/rpm/">http://ftp.kde.org/unstable/distribution/2.1-beta1/rpm/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.

KDE 2.1-beta1 requires qt-2.2.1, the free version of which is available
from the above locations usually under the name qt-x11-2.2.1, although
qt-2.2.3 is recommended. KDE 2.1-beta1 will not work with versions of Qt
older than 2.2.1.

At the time of this release, pre-compiled packages are available for:

<UL>

<li><a href="http://ftp.kde.org/unstable/distribution/2.1beta1/rpm/Mandrake/7.2/">Linux Mandrake 7.2 (i386)</a></li>
<li><a href="http://ftp.kde.org/unstable/distribution/2.1beta1/rpm/SuSE/7.0-i386/">SuSE Linux 7.0 (i386)</a></li>
</UL>

Please check the servers periodically for pre-compiled packages for other
distributions. More binary packages will become available over the
coming days and weeks.

#### About KDE

KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.

For more information about KDE, please visit KDE's <a href="http://www.kde.org/whatiskde/">web site</a>.

<hr /><font size=2>

_Trademarks Notices._
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
Java is a trademark of Sun Microsystems, Inc.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.</font>
<br>

<hr /><table border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
    Kurt Granroth <br>
    
  [granroth@kde.org](mailto:granroth@kde.org)
    <br>
    (1) 480 732 1752<br>&nbsp;<br>
    Andreas Pour<br>
    [pour@kde.org](mailto:pour@kde.org)<br>
    (1) 718-456-1165
  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<BR>
(44) 1225 837409

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Martin Konold<br>
    
  [konold@kde.org](mailto:konold@kde.org)
    <br>
    (49) 179 2252249
  </td>
</tr>
</table>