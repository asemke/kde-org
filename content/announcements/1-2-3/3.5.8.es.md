---
aliases:
- ../announce-3.5.8
custom_about: true
date: '2007-10-16'
title: Anuncio de lanzamiento de KDE 3.5.8
---

<h3 align="center">
El proyecto KDE lanza la octava versión que mejora las traducciones y servicios del escritorio líder de software libre.
</h3>

<p align="justify">
  KDE 3.5.8 incluye traducciones a 65 idiomas, mejoras en la <i>suite</i> KDE PIM y otras aplicaciones.
</p>

<p align="justify">
El <a href="http://www.kde.org/">Proyecto KDE</a> ha anunciado
 hoy la inmediata disponibilidad de KDE 3.5.8, una versión de mantenimiento para la última
 generación del más avanzado y potente escritorio <em>libre</em> para GNU/Linux y otros UNIXes. KDE ahora incorpora
 traducciones a 65 idiomas, haciéndolo disponible a más gente que la mayoría del software no libre y pudiendo ser fácilmente
 extendido para admitir otros idiomas por comunidades que deseen contribuir al proyecto de código abierto.
</p>

<p align="justify">

Mientras el principal objetivo de los desarrolladores es terminar KDE 4.0, la serie
estable 3.5 sigue siendo el escritorio a usar por el momento. Está probada, es estable y está bien
soportada. La versión 3.5.8, con sus literalmente cientos de correcciones, ha mejorado
de nuevo la experiencia de los usuarios. Los principales objetivos de las mejoras de KDE 3.5.8 son:

  <ul>
      <li>
  Mejoras en Konqueror y su componente de navegación web KHTML. Se han corregido errores
  en el manejo de conexiones HTTP y KHTML ha mejorado el soporte de algunas funcionalidades
  de CSS para ser más compatible con los estándares.
      </li>
      <li>
  En el paquete kdegraphics, muchísimas correcciones en el visor de PDF de KDE y Kolourpaint, una aplicación de pintura, se
  han incluido en esta versión.
      </li>
      <li>
  La <i>suite</i> KDE PIM, como es habitual, ha recibido numerosas correcciones de estabilidad, cubriendo el cliente
  de correo de KDE KMail, el organizador KOrganizer y otros programas.
      </li>
  </ul>
</p>

<p align="justify">
Para ver una lista más detallada de las mejoras realizadas desde 
<a href="http://www.kde.org/announcements/announce-3.5.7-es"> el lanzamiento de KDE 3.5.7</a> el 22 de mayo de 2007, por favor visite el <a href="http://www.kde.org/announcements/changelogs/changelog3_5_7to3_5_8">registro de cambios de KDE 3.5.8</a>.
</p>

<p align="justify">
KDE 3.5.8 incluye un escritorio básico y otros quince paquetes (<acronym title="Gestor de Información Personal">PIM</acronym>, administración, red, entretenimientos educativos, utilidades, multimedia, juegos, material gráfico, desarrollo web y más). Las premiadas herramientas de KDE están disponibles en 65 idiomas.
</p>

<h4>
Distribuciones que incluyen KDE
</h4>
<p align="justify">
La mayor parte de las distribuciones de Linux y sistemas operativos UNIX no incorporan
de forma inmediata las nuevas versiones de KDE, pero integrarán KDE 3.5.8 en sus
próximas versiones. Compruebe <a href="http://www.kde.org/download/distributions">esta
lista</a> para ver qué distribuciones incluyen KDE.
</p>

<h4>
Instalar los paquetes binarios de KDE 3.5.8
</h4>
<p align="justify">
<em>Creadores de paquetes</em>.
Algunos proveedores de sistemas operativos han proporcionado generosamente
paquetes binarios de KDE 3.5.8 para algunas versiones de su distribución, y en
otros casos comunidades de voluntarios lo han hecho.
Algunos de estos paquetes binarios están disponibles para su libre descarga
en el servidor de descargas de KDE en 
<a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.8/">http://download.kde.org</a>.
Paquetes binarios adicionales, así como actualizaciones de los paquetes ahora
disponibles, pueden estar disponibles en las próximas semanas.
</p>

<p align="justify">
<a id="package_locations"><em>Localizaciones de paquetes</em></a>.
Para ver una lista actualizada de paquetes binarios disponibles de los que el Proyecto
KDE ha sido informado, por favor visite la <a href="/info/3.5.8">página
de información de KDE 3.5.8</a>.
</p>

<h4>
Compilar KDE 3.5.8
</h4>
<p align="justify">
  <a id="source_code"></a><em>Código fuente</em>.
  El código fuente completo de KDE 3.5.8 puede ser
  <a href="http://download.kde.org/stable/3.5.8/src/">descargado libremente</a>.
  Hay disponibles instrucciones acerca de compilar e instalar KDE 3.5.8
  en la <a href="/info/3.5.8">página de información de KDE 3.5.8</a>.
</p>

<h4>
Ayudar a KDE
</h4>
<p align="justify">
KDE es un proyecto de <a href="http://www.gnu.org/philosophy/free-sw.html">Software Libre</a>
que existe y crece sólo mediante la ayuda de muchos voluntarios que donan su tiempo y esfuerzo. KDE siempre está buscando nuevos voluntarios y colaboradores, bien para ayudar programando, arreglando fallos o informando de ellos, escribiendo documentación, traducciones, promocionándolo, donando dinero, etc. Todos los colaboradores son gratamente apreciados y esperados con mucho entusiasmo. Por favor, lea la página <a href="/community/donations/">Ayudar a KDE</a> para más información.
</p>

<p align="justify">
¡Esperamos noticias suyas pronto!
</p>

<h4>
  Acerca de KDE
</h4>
<p align="justify">
KDE es un <a href="/community/awards/">premiado</a> proyecto independiente formado por cientos de
desarrolladores, traductores, artistas y otros profesionales de todo el mundo, que colaboran
a través de Internet para crear y distribuir libremente un entorno de escritorio y oficina
sofisticado, personalizable y estable, basado en componentes, con arquitectura transparente
a la red y que ofrece una excepcional plataforma de desarrollo.
</p>

<p align="justify">
KDE ofrece un escritorio estable y maduro, incluyendo un navegador de última generación
(<a href="http://konqueror.kde.org/">Konqueror</a>), una <i>suite</i> de gestión de 
información personal (<a href="http://kontact.org/">Kontact</a>), una completa <i>suite</i>
ofimática (<a href="http://www.koffice.org/">KOffice</a>), un gran conjunto de aplicaciones
de red, utilidades y un entorno de desarrollo eficiente e intuitivo, que incluye el
excelente IDE <a href="http://www.kdevelop.org/">KDevelop</a>.
</p>

<p align="justify">
KDE es una prueba en funcionamiento de que el modelo de desarrollo "estilo bazar" del
código abierto puede producir tecnologías de primer nivel, iguales o superiores al más
complejo software comercial.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Marcas registradas.</em>
  KDE<sup>&#174;</sup> y el K Desktop Environment<sup>&#174;</sup> son marcas registradas de
  KDE e.V.

Linux es una marca registrada de Linus Torvalds.

UNIX es una marca registrada de The Open Group en los Estados Unidos y otros países.

Todas las otras marcas registradas y copyrights mencionados en este anuncio son propiedad
de sus respectivos dueños.
</font>

</p>

<hr />
